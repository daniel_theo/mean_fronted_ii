import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnackbarErrorOfflineComponent } from './snackbar-error-offline.component';

describe('SnackbarErrorOfflineComponent', () => {
  let component: SnackbarErrorOfflineComponent;
  let fixture: ComponentFixture<SnackbarErrorOfflineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnackbarErrorOfflineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackbarErrorOfflineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
