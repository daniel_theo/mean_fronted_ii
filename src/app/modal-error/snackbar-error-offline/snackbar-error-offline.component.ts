import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-snackbar-error-offline',
  templateUrl: './snackbar-error-offline.component.html',
  styleUrls: ['./snackbar-error-offline.component.css']
})
export class SnackbarErrorOfflineComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
