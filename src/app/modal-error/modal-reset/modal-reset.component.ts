import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-reset',
  templateUrl: './modal-reset.component.html',
  styleUrls: ['./modal-reset.component.css']
})
export class ModalResetComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  login(){
    window.location.replace('/login');
  }

}
