import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorProfileComponent } from './error-profile.component';

describe('SnackbarErrorOfflineComponent', () => {
  let component: ErrorProfileComponent;
  let fixture: ComponentFixture<ErrorProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
