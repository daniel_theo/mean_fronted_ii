import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-profile',
  templateUrl: './error-profile.component.html',
  styleUrls: ['./error-profile.component.css']
})
export class ErrorProfileComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

   logout() {
    localStorage.clear();
    window.location.replace('/login');
  }

}
