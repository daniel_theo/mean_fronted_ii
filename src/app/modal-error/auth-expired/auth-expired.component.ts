import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-auth-expired',
  templateUrl: './auth-expired.component.html',
  styleUrls: ['./auth-expired.component.css']
})
export class AuthExpiredComponent implements OnInit {

  constructor(
    public data: DataService
  ) { }

  ngOnInit() {
  }

  // logout when reset password success
  login(){
    this.data.user = undefined;
    localStorage.clear();
    window.location.replace('/login');
  }

}
