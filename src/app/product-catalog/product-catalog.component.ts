import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-catalog',
  template: `
   <router-outlet></router-outlet>
  `,
  styles: []
})
export class ProductCatalogComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}