import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-modal-stock',
  templateUrl: './modal-stock.component.html',
  styleUrls: ['./modal-stock.component.css']
})
export class ModalStockComponent implements OnInit {
  isDisabled = true;
  panelOpenState = false;
  incomingItem = [];
  total = 0;
  url= this.rest.link_url();
  customCollapsedHeightDsbl: string = '40px';

  constructor(
    public dialogRef: MatDialogRef<ModalStockComponent>,
    @Inject(MAT_DIALOG_DATA) public dataitem: any,
    public data: DataService,
    private rest: RestApiService
  ) { }

  ngOnInit() {
    this.incoming();
  }

  async incoming(){
    try{
        const data = await this.rest.get(
          `${this.url}/api-item/list-incoming-item/`+this.dataitem.items.No
        );
        this.incomingItem = data['item'];
        this.total = this.incomingItem.reduce((acc,cur) => acc + cur.OutsQty,0);
    }catch (error) {
      this.data.error(error['message']);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
