import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { MaterialAppModule } from '../ngmaterial.module';

import { ProductCatalogRoutingModule } from './product-catalog-routing.module';

import { ProductCatalogComponent } from './product-catalog.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

import { DialogCustomerComponent } from './dialog-customer/dialog-customer.component';
import { ProductListComponent } from './product-list/product-list.component';
// import { LazyLoadImagesModule } from 'ngx-lazy-load-images';
import { SnackbarTrueComponent } from './snackbar-product/snackbar-true/snackbar-true.component';
import { SnackbarFalseComponent } from './snackbar-product/snackbar-false/snackbar-false.component';
import { ModalStockComponent } from './modal-stock/modal-stock.component';
import { ModalOrderComponent } from './modal-order/modal-order.component';
import { ScrollingModule,ScrollDispatchModule  } from '@angular/cdk/scrolling';

@NgModule({
  imports: [
    CommonModule,
    ProductCatalogRoutingModule,
    MaterialAppModule,
    ReactiveFormsModule,
    FormsModule,
    // LazyLoadImagesModule,
    ScrollingModule,
    ScrollDispatchModule
  ],
  entryComponents: [
    SnackbarTrueComponent,
    SnackbarFalseComponent,
    ProductDetailComponent,
    ModalStockComponent,
	  ModalOrderComponent,
    DialogCustomerComponent
  ],
  declarations: [
    ProductDetailComponent,
		ProductListComponent,
	  ProductCatalogComponent,
	  SnackbarTrueComponent,
	  SnackbarFalseComponent,
	  ModalStockComponent,
	  ModalOrderComponent,
    DialogCustomerComponent
  ]
})
export class ProductCatalogModule { }
