import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RestApiService } from '../../rest-api.service';
import { SalesOrderService } from '../../sales-order.service';
import { DataService } from '../../data.service';

import {MatSnackBar} from '@angular/material';

import { SnackbarTrueComponent } from '../snackbar-product/snackbar-true/snackbar-true.component';
import { SnackbarFalseComponent } from '../snackbar-product/snackbar-false/snackbar-false.component';

@Component({
    selector: 'app-modal-order',
    templateUrl: './modal-order.component.html',
    styleUrls: ['./modal-order.component.css']
})
export class ModalOrderComponent implements OnInit {
    qtyVar :any = {};
    qty = 0;
    dataOrder:any={};
    dataGroup:any={};
    dataLclStorage:any ={};

    price = 0;
    dataItemVar= [];
    dataItemGdg= [];

    url= this.rest.link_url();

    keyQty :any;
    accQty :any = 0;
    actualStock = 0;

    constructor(
        public dialogRef: MatDialogRef<ModalOrderComponent>,
        @Inject(MAT_DIALOG_DATA) public dataitem: any,
        public data: DataService,
        private rest: RestApiService,
        public snackBar: MatSnackBar,
        private sales_order : SalesOrderService
    )
    { this.accQty= 0;}

    ngOnInit() {
        this.Itemdetail();
        this.dataLclStorage =  JSON.parse(localStorage.getItem("order_draft"));
    }

    async Itemdetail(){
        try {
            this.dataItemGdg =  this.dataitem;
            this.actualStock =  this.dataitem.items.Inv +
                                this.dataitem.items.InvToday-
                                this.dataitem.items.soQty-
                                this.dataitem.items.ToOut-
                                this.dataitem.items.ToOutToday-
                                this.dataitem.items.NotPosted-
                                this.dataitem.items.NotPostedToday-
                                this.dataitem.items.ShipTodayToOutNotToday
            if (isNaN(this.actualStock)){
                this.actualStock = this.dataitem["items"].actualStock;
            }
        
            this.qty = 0;
            
            this.price = this.dataitem.items.salesPrice.toLocaleString(['id']);
        } catch (error) {
            this.data.error(error['message']);
        }
    }

    async order() {
        let index =0;
        let countQty = 0;
        this.dataOrder.itemNo    = this.dataitem.items.No;
        this.dataOrder.desc      = this.dataitem.items.Description;
        this.dataOrder.desc2     = this.dataitem.items.Description2;
        this.dataOrder.UOM       = this.dataitem.items.UOM;
        
        if (this.data.user.company === "UNICODISTRIBUSI"){
            this.dataOrder.price    = this.dataitem.items.Price1;
            this.dataOrder.qty      = this.qty;
            await this.sales_order.itemLocalStorage(this.dataOrder);
            this.qty = 0;
            countQty++;
        }else{
            switch (this.data.user.company) {
            case "SENTRALTUKANG":
                switch (this.data.user.dept_code) {
                case "PDG":
                    if(this.dataLclStorage !== null){
                    switch (this.dataLclStorage.discGroup) {
                        case "CASH":
                        this.dataOrder.price    = this.dataitem.items.Price1;  
                        break;
                        case "PDG_KRDT_M":
                        this.dataOrder.price    = this.dataitem.items.Price2; 
                        break;
                        case "PDG_KRDT":
                        this.dataOrder.price    = this.dataitem.items.Price3;  
                        break;
                        default:
                        this.dataOrder.price    = this.dataitem.items.Price3;
                        break;
                    }
                    }else{
                    this.dataOrder.price    = this.dataitem.items.Price3;
                    }
                break;
                case "PKU":
                    if(this.dataLclStorage !== null){
                    switch (this.dataLclStorage.discGroup) {
                        case "CASH": 
                        this.dataOrder.price    = this.dataitem.items.Price2; 
                        break;
                        case "PKU_M_CASH":
                        this.dataOrder.price    = this.dataitem.items.Price1;  
                        break ; 
                        case "PKU_M_KRDT":
                        this.dataOrder.price    = this.dataitem.items.Price3; 
                        break;
                        default:
                        this.dataOrder.price    = this.dataitem.items.Price3; 
                        break;
                    }
                    }else{
                    this.dataOrder.price    = this.dataitem.items.Price3;  
                    } 
                break;
                default:
                    this.dataOrder.price    = this.dataitem.items.Price1;
                break;
                }
                break;
            
            default:
                this.dataOrder.price    = this.dataitem.items.Price1;
            break;
            }

            this.dataOrder.qty      = this.qty;
            await this.sales_order.itemLocalStorage(this.dataOrder);
            this.qty = 0;
            countQty++;
        }
        if(countQty > 0){
        this.snackBar.openFromComponent(SnackbarTrueComponent, {
                duration: 1000,
            });
        }else {
        this.snackBar.openFromComponent(SnackbarFalseComponent, {
                duration: 1000,
            });
        }
        this.dialogRef.close();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    removeIcon(arr){
        this.qty -= 1;
        this.CalculateQty();
    }

    addIcon(arr){
        this.qty += 1 ;
        if (this.qty > arr){
        this.qty = 0;
        }
        this.CalculateQty();
    }

    removeIconUSD(arr){
        this.qtyVar[arr.variantCode] = this.qtyVar[arr.variantCode] - 1;

        if (this.qtyVar[arr.variantCode] >arr.actualStock){
        this.qtyVar[arr.variantCode] = 0;
        }
        this.CalculateQtyUsd();
    }

    addIconUSD(arr){
        console.log(arr);
        this.qtyVar[arr.variantCode] = this.qtyVar[arr.variantCode] + 1;
        if (this.qtyVar[arr.variantCode] >arr.actualStock){
        this.qtyVar[arr.variantCode] = 0;
        }
        this.CalculateQtyUsd();
    }

    updateQty(arr){
        if (this.qty >arr){
        this.qty = arr;
        } else if(this.qty< 0){
        this.qty = 0; 
        }
        this.CalculateQty();
    }

    updateQtyUSD(arr){
        if (this.qtyVar[arr.variantCode] >arr.actualStock){
        this.qtyVar[arr.variantCode] = arr.actualStock;
        } else if(this.qtyVar[arr.variantCode] < 0){
        this.qtyVar[arr.variantCode] = 0; 
        }
        this.CalculateQtyUsd();
    }

    CalculateQtyUsd(){
        this.accQty = Object.values(this.qtyVar).reduce((acc: number, cur: number) => acc + cur ,0);
    }
    CalculateQty(){
        this.accQty = this.qty;
    }
}
