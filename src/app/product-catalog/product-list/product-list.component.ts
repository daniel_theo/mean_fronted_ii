import { Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';
import { Router,ActivatedRoute ,Params } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef } from '@angular/material';
import { SalesOrderService } from '../../sales-order.service';

import { Observable, Subject, throwError, BehaviorSubject, Subscription} from 'rxjs';


import { ModalStockComponent } from '../modal-stock/modal-stock.component';
import { ModalOrderComponent } from '../modal-order/modal-order.component';

import { DialogCustomerComponent } from '../dialog-customer/dialog-customer.component';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';

import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  providers: [DashboardComponent]
})
export class ProductListComponent {
    @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
    customCollapsedHeight: string = '40px';
    customCollapsedHeightDsbl: string = '40px';
    customExpandedHeight: string = '40px';
    
    customCollapsedHeightPrice: string = '30px';
    customExpandedHeightPrice: string = '30px';

    item: Object;
    searchTerm$ = new Subject<string>();
    page = 1 ;
    dataLclStorage:any={};
    dataItem:any ={};
    dataScroll:any ={};
    Scrool = 1;


    url= this.rest.link_url();

    filter_description = "";
    filter = "";
    
    dataArr =[];
    public ds;

    private _mobileQueryListener: () => void;
    constructor(
       public data: DataService,
       public sales_order : SalesOrderService,
       private rest: RestApiService,
       private router: Router,
       private _location: Location,
       private activatedRoute : ActivatedRoute,
       public dsbComponent: DashboardComponent,
       public dialog: MatDialog,
     ) {
      this.data.getProfile;
        if (JSON.parse(localStorage.getItem("product_list"))!== null && typeof JSON.parse(localStorage.getItem("product_list"))!== undefined){
        this.filter_description = JSON.parse(localStorage.getItem("product_list")).description;
        
      }
      this.page               = 1;
    }
    // back() {
    //   this._location.back();
    // } 
  
    async ngOnInit() { 
      this.item = null;
      const dataNo = await this.rest.get(
        `${this.url}/api-item/list-item-scrolling-no?description=${this.filter_description}`
        );
      // if(dataNo['item']){
      //   localStorage.setItem('product_list', JSON.stringify({...JSON.parse(localStorage.getItem("product_list")),description:this.filter_description,scrolling: 0}));
      //   this.Scrool = 0;
      // }
      if(dataNo['success']){
        if (dataNo['item'].length == 0){
          localStorage.setItem('product_list', JSON.stringify({...JSON.parse(localStorage.getItem("product_list")),description:this.filter_description,scrolling: 0}));
          this.Scrool = 0;
          this.filter = this.filter_description;
        } else {
          localStorage.setItem('product_list', JSON.stringify({...JSON.parse(localStorage.getItem("product_list")),description:this.filter_description,scrolling: dataNo['item'][0].RowNumber}));
          this.Scrool = dataNo['item'][0].RowNumber;
        }
        if (this.Scrool < 20){
          this.ds = null;
            let dataArrItem =[];
            const data = await this.rest.get(
               `${this.url}/api-item/list-item-pagination?page=1&description=${this.filter_description}`
             );
            if (data['success']){
                 data['item'].map(data=> dataArrItem.push(
                  {  ...data,
                      Price3_view:data.Price3.toLocaleString(['ban', 'id']),
                      Price2_view:data.Price2.toLocaleString(['ban', 'id']),
                      Price1_view:data.Price1.toLocaleString(['ban', 'id'])
                  })    
                );
                this.item = dataArrItem  ;
            }else{
              this.data.error(data['message']);
            }     
        }else{            
            // this.ds = new MyDataSource(this.rest);
            this.ds = new MyDataSource(this.rest, this.Scrool, this.filter_description);
        }
      }else{
          this.data.error(dataNo['message']);
      }
      try{
          await this.viewPort.scrollToIndex(2.2, 'auto');
          await this.viewPort.scrollToIndex(1.5, 'smooth');
          this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));

          await this.viewPort.scrollToIndex(0, 'smooth');
      }catch (error) {
      this.data.error(error['message']);

    }
  }
  async processSearchItem(event: any) {
    try{
      await this.ngOnInit();         
    } catch (error) {
      this.data.error(error['message']);
    }   
  }

  // destroy search
  ngOnDestroy() {
    this.searchTerm$.unsubscribe();
    // localStorage.removeItem('product_list');
  }

  // clear search
  ClearSearch(){
    this.searchTerm$.next("");
    this.filter_description='';
  }

  openDialogStock(items): void {
    const dialogRef = this.dialog.open(ModalStockComponent, {
      height: 'auto',
      width: '600px',
      data: {
        items
      }
    });
  }

  openDialogOrder(items): void {
    const dialogRef = this.dialog.open(ModalOrderComponent, {
      height: '300px',
      width: '600px',
      data: {
        items
      },
      autoFocus: false 

    });
  }
  
      // open dialog customer
  openDialogCustomer(): void {
    const dialogRef = this.dialog.open(DialogCustomerComponent, {
      height: '500px',
      width: '800px'
    });

    //get data from localStorage order draft when selected in dialog customer
    dialogRef.afterClosed().subscribe(arr => {
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
    });
  }
  
}
export class MyDataSource extends DataSource<string | undefined> {

  private length   = this.scrollParam;
  private pageSize = 20;
  private cachedData = Array.from<string>({length: this.length});
  private fetchedPages = new Set<number>();
  private dataStream = new BehaviorSubject<(any | undefined)[]>(this.cachedData);
  private subscription = new Subscription();

  constructor(
      private rest: RestApiService,
      private scrollParam,
      private descriptionParam) {
    super();
  }
  filter_description="";
  dataArr =[];
  connect(collectionViewer: CollectionViewer): Observable<(any | undefined)[]> {
    this.subscription.add(collectionViewer.viewChange.subscribe(range => {   
      const startPage = this.getPageForIndex(range.start);
      const endPage = this.getPageForIndex(range.end);
      for (let i = startPage; i <= endPage; i++) {
        this.fetchPage(i);
      }
    }));
    return this.dataStream;
  }

  disconnect(): void {
    this.subscription.unsubscribe();
  }

  private getPageForIndex(index: number): number {
    return Math.floor(index / this.pageSize);
  }

  private fetchPage(page: number) {
    if (this.fetchedPages.has(page)) {      
      return;
    }
    this.filter_description = this.descriptionParam;
    // if (JSON.parse(localStorage.getItem("product_list"))!== null && typeof JSON.parse(localStorage.getItem("product_list"))!== undefined){
    //     this.filter_description = JSON.parse(localStorage.getItem("product_list")).description;
    //   }
    this.fetchedPages.add(page);
    this.rest.getData((page * this.pageSize)+1,this.filter_description ).subscribe(data => { 
    this.dataArr=[];
     data['item'].map(data=> this.dataArr.push(
        {  ...data,
          Price3_view:data.Price3.toLocaleString(['ban', 'id']),
          Price2_view:data.Price2.toLocaleString(['ban', 'id']),
          Price1_view:data.Price1.toLocaleString(['ban', 'id'])
        })
    );
    this.cachedData.splice(page * this.pageSize, this.pageSize,...this.dataArr);
      this.dataStream.next(this.cachedData);
    });
  }
}