import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../rest-api.service';
import { SalesOrderService } from '../../sales-order.service';
import { DataService } from '../../data.service';
import { ActivatedRoute,Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef } from '@angular/material';


import { ModalOrderComponent } from '../modal-order/modal-order.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  itemId: any;
  price = 0;
  price1 =0;
  price2 =0;
  price3 =0;
  description ='' ;
  description2 ='';
  
  customCollapsedHeightPrice: string = '30px';
  customExpandedHeightPrice: string = '30px';

  dataItem: any = {};
  dataItems: any = {};
  filter_description = "";
  url= this.rest.link_url();
  prod_list_page ='';
  prod_list_description ='';

  constructor(
    private _location: Location,
    public data: DataService,
    private rest: RestApiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public sales_order : SalesOrderService,
     public dialog: MatDialog

  ) {
    this.sales_order.chartQty;
    this.dataItems =  JSON.parse(localStorage.getItem("order_draft"));
  }

  async ngOnInit() {
      this.activatedRoute.params.subscribe(res => {
          this.itemId = res['No'];  
          this.Itemdetail();
        });
   }
  
   // item detail product
   async Itemdetail(){
       try {
           const dataEx= await this.rest.get(
               `${this.url}/api-item/list-item-detail/`+this.itemId
           );
           this.dataItem = dataEx['item'][0];
           
           this.description = this.data.toTitleCase(this.dataItem.Description);
           this.description2 = this.data.toTitleCase(this.dataItem.Description2);
           this.price1 = this.dataItem.Price1.toLocaleString(['id']);
           this.price2 = this.dataItem.Price2.toLocaleString(['id']);
           this.price3 = this.dataItem.Price3.toLocaleString(['id']);
           
           if(localStorage.getItem('product_list')){
             this.prod_list_page = JSON.parse(localStorage.getItem('product_list')).page;
             this.prod_list_description = JSON.parse(localStorage.getItem('product_list')).description;
           }
        } catch (error) {
           this.data.error(error['message']);
       }
   }

   // windows location back
    goback() {
        this._location.back();
    }

    openDialogOrder(items): void {
      const dialogRef = this.dialog.open(ModalOrderComponent, {
        height: '300px',
        width: '600px',
        data: {
          items
        },
        autoFocus: false 
      });
    }

}
