import { ProductCatalogModule } from './product-catalog.module';

describe('ProductCatalogModule', () => {
  let productCatalogModule: ProductCatalogModule;

  beforeEach(() => {
    productCatalogModule = new ProductCatalogModule();
  });

  it('should create an instance', () => {
    expect(productCatalogModule).toBeTruthy();
  });
});
