import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductCatalogComponent } from './product-catalog.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductListComponent } from './product-list/product-list.component';

const routes: Routes = [
{
	path: '',
    component: ProductCatalogComponent,
     children: [
      {
        path: 'list',
        component: ProductListComponent
      },
      {
        path: 'list/:this.cari',
        component: ProductListComponent
      },
      {
        path: 'detail/:No',
        component: ProductDetailComponent
      }
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductCatalogRoutingModule { }
