import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialAppModule } from './ngmaterial.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MessageComponent } from './message/message.component';

import { RestApiService } from './rest-api.service';
import { SalesOrderService } from './sales-order.service';
import { CustomerPriceService } from './customer-price.service';
import { DataService } from './data.service';
import { AuthGuardService } from './auth-guard.service';
import { HttpModule } from '@angular/http';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtTokenService } from './auth/jwt-token.service';
import { AuthExpiredComponent } from './modal-error/auth-expired/auth-expired.component';
import { NgStickyModule } from 'ng-sticky';
import { SnackbarErrorOfflineComponent } from './modal-error/snackbar-error-offline/snackbar-error-offline.component';
import { ErrorProfileComponent } from './modal-error/error-profile/error-profile.component';
import { ScrollingModule,ScrollDispatchModule  } from '@angular/cdk/scrolling';
import { SubmittedComponent } from './sales-order/order-product/submitted/submitted.component';

@NgModule({
  declarations: [
    AppComponent,
    MessageComponent,
    AuthExpiredComponent,
    SnackbarErrorOfflineComponent,
    ErrorProfileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    MaterialAppModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    NgStickyModule,
    ScrollingModule,
    ScrollDispatchModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtTokenService,
      multi: true
    },
    RestApiService,
    DataService,
    AuthGuardService,
    SalesOrderService,
    CustomerPriceService
  ],
  entryComponents: [
    AuthExpiredComponent,SnackbarErrorOfflineComponent,ErrorProfileComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
