import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {switchMap, debounceTime, map, distinctUntilChanged, catchError} from 'rxjs/operators';

import { MatDialog, MatDialogRef } from '@angular/material';

import { ErrorBlockedComponent } from './modal-error/error-blocked/error-blocked.component';


@Injectable({
  	providedIn: 'root'
})
export class RestApiService {
  	// baseUrl: string = 'https://api-dev.uapps.id/api-item/list-item-pagination?';
    // baseUrl: string = 'http://localhost:8081/api-item/list-item-pagination?';
    baseUrl: string = 'https://api.uapps.id/api-item/list-item-pagination?';
  	// queryUrl: string = 'page=1&description=';

  	// baseUrl_customer : string = 'https://api-dev.uapps.id/api-customer/list-customer-pagination?';
    // baseUrl_customer : string = 'http://localhost:8081/api-customer/list-customer-pagination?';
    baseUrl_customer : string = 'https://api.uapps.id/api-customer/list-customer-pagination?';
  	queryUrl_customer : string = 'page=1&name=';

    // baseUrl_salesOrder : string = 'https://api-dev.uapps.id/api-sales-order/order';
    // baseUrl_salesOrder : string = 'http://localhost:8081/api-sales-order/order';
    baseUrl_salesOrder : string = 'https://api.uapps.id/api-sales-order/order';
    queryUrl_salesOrder : string = '?param=';


	  dataLocalStorage:any={};

  	constructor(
      private http: HttpClient,
      public dialog: MatDialog
    ) {}

   	getHeaders() {
    	const token = localStorage.getItem('token');
    	return token ? new HttpHeaders().set('Authorization', token) : null;
  	}


    get(link: string) {
    	return this.http.get(link, { headers: this.getHeaders() }).toPromise();
  	}

   	post(link: string, body: any) {
    	return this.http.post(link, body, { headers: this.getHeaders() }).toPromise();
  	}

    put(link: string, body: any) {
      return this.http.put(link, body, { headers: this.getHeaders() }).toPromise();
    }

    delete(link: string) {
    	return this.http.delete(link, { headers: this.getHeaders() }).toPromise();
  	}
  	link_url(){
  		// return 'https://api-dev.uapps.id';
      // return 'http://localhost:8081';
      return 'https://api.uapps.id';
  	}

    user_getProfile(){
        return this.http.get(`${this.link_url()}/api-accounts`,{ headers: this.getHeaders() });
    }

  getCustomerPriceSti(itemNo, discGroup) {
    return this.http.get(`${this.link_url()}/api-item/cek-customer-price/${itemNo}/${discGroup}`, { headers: this.getHeaders() });
    // `${this.url}/api-item/cek-customer-price-sti/${item.itemNo}/${arr.DiscGroup}`,
  }
  getCustomerPrice(itemNo, discGroup) {
    // return this.http.get(`${this.link_url()}/api-item/cek-customer-price/${itemNo}/${discGroup}`, { headers: this.getHeaders() });
    return this.http.get(`${this.link_url()}/api-item/cek-customer-price/${itemNo}`, { headers: this.getHeaders() });
  }

    // // search data items
    // search(terms: Observable<string>) {
    //   return terms.pipe(debounceTime(500),
    //     distinctUntilChanged(),
    //     switchMap(term => this.searchEntries(term)));
    // }

    // // search entry data items
    // searchEntries(term) {
    // 	this.dataLocalStorage.page = 1 ;
    // 	this.dataLocalStorage.description = term;
    //   	localStorage.setItem('product_list', JSON.stringify(this.dataLocalStorage));
    //   	return this.http
    //       .get(this.baseUrl + this.queryUrl + term,{ headers: this.getHeaders() })
    //       .pipe(map((res: Response) => res));
    // }

    // search customer
    search_customer(terms: Observable<string>) {
      return terms.pipe(debounceTime(500),
        distinctUntilChanged(),
        switchMap(term => this.searchEntries_customer(term)));
    }

    // search entry customer
    searchEntries_customer(term) {
      return this.http
          .get(this.baseUrl_customer + this.queryUrl_customer + term,{ headers: this.getHeaders() })
          .pipe(map((res: Response) => res));
    }

    //search salesOrder
    search_salesOrder(terms: Observable<string>) {
      return terms.pipe(debounceTime(500),
        distinctUntilChanged(),
        switchMap(term => this.searchEntries_salesOrder(term)));
    }
     // search entry data salesOrder
    searchEntries_salesOrder(term) {
      return this.http
          .get(this.baseUrl_salesOrder + this.queryUrl_salesOrder + term,{ headers: this.getHeaders() })
          .pipe(map((res: Response) => res));
    }
    getData(since,description) {
      return this.getUsers(since,description)
        .pipe(
          map(users => users)
        )
    }
    getUsers(since=0,description = '') : Observable<any[]> {
      // return this.http.get<any[]>(`https://api-dev.uapps.id/api-item/list-item-scrolling?page=${since}&description=${description}` ,{ headers: this.getHeaders()})
      // return this.http.get<any[]>(`http://localhost:8081/api-item/list-item-scrolling?page=${since}&description=${description}` ,{ headers: this.getHeaders()})
      return this.http.get<any[]>(`https://api.uapps.id/api-item/list-item-scrolling?page=${since}&description=${description}` ,{ headers: this.getHeaders()})
      .pipe(
      // tap(() => console.log(`fetched users ${since}`)),
        catchError(this.handleError('getUsers'))
      ) as Observable<any>;
    }
    getDataSo(since,startingDate,endingDate,description) {
      return this.getUsersSo(since,startingDate,endingDate,description)
        .pipe(
          map(users => users)
        )
    }
    getUsersSo(since=0,startingDate,endingDate,description = '') : Observable<any[]> {
      // return this.http.get<any[]>(`https://api-dev.uapps.id/api-sales-order/orderScrolling?page=${since}&startingDate=${startingDate}&endingDate=${endingDate}&description=${description}` ,{ headers: this.getHeaders()})
      //  return this.http.get<any[]>(`http://localhost:8081/api-getorder/order-scrolling?page=${since}&startingDate=${startingDate}&endingDate=${endingDate}&description=${description}` ,{ headers: this.getHeaders()})
       return this.http.get<any[]>(`https://api.uapps.id/api-getorder/order-scrolling?page=${since}&startingDate=${startingDate}&endingDate=${endingDate}&description=${description}` ,{ headers: this.getHeaders()})
      .pipe(
        catchError(this.handleError('getUsers'))
      ) as Observable<any>;
    }
    getDataCustomer(since,description) {
      return this.getUsersCustomer(since,description)
        .pipe(
          map(users => users)
        )
    }
    getUsersCustomer(since=0,description = '') : Observable<any[]> {
      // return this.http.get<any[]>(`https://api-dev.uapps.id/api-customer/list-customer-scrolling?page=${since}&description=${description}` ,{ headers: this.getHeaders()})
      // return this.http.get<any[]>(`http://localhost:8081/api-customer/list-customer-scrolling?page=${since}&description=${description}` ,{ headers: this.getHeaders()})
      return this.http.get<any[]>(`https://api.uapps.id/api-customer/list-customer-scrolling?page=${since}&description=${description}` ,{ headers: this.getHeaders()})
      .pipe(
        catchError(this.handleError('getUsers'))
      ) as Observable<any>;
    }
    private handleError<T> (operation = 'operation') {
    return (error: HttpErrorResponse): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      const message = (error.error instanceof ErrorEvent) ?
        error.error.message :
      `server returned code ${error.status} with body "${error.error}"`;

      // TODO: better job of transforming error for user consumption
      throw new Error(`${operation} failed: ${message}`);
    };  
  }

}
