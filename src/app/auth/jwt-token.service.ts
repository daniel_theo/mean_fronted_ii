import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
  HttpResponse,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

import {MatSnackBar} from '@angular/material';

import { MatDialog, MatDialogRef } from '@angular/material';

import { AuthExpiredComponent } from '../modal-error/auth-expired/auth-expired.component';
import { SnackbarErrorOfflineComponent } from '../modal-error/snackbar-error-offline/snackbar-error-offline.component';
import { ErrorProfileComponent } from '../modal-error/error-profile/error-profile.component'

@Injectable({
  providedIn: 'root'
})
export class JwtTokenService implements HttpInterceptor {

  constructor(
    public auth: AuthService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any ) => {
      if (err instanceof HttpErrorResponse) {
         if (!window.navigator.onLine) {
           const snack = this.snackBar.openFromComponent(SnackbarErrorOfflineComponent,{           
          });          
          
         }else if (err.status === 401 || err.status === 403) {
          const dialogRef = this.dialog.open(AuthExpiredComponent, {
            width: '90%'
          });
          this.auth.collectFailedRequest(request);
        } else if(err.status === 0){
          // console.log(err.message);
            const snack = this.snackBar.openFromComponent(ErrorProfileComponent,{     
              duration: 10000      
          });
        }
      }
    });
  }
}
