import { Injectable } from '@angular/core';
import { RestApiService } from './rest-api.service';
import { DataService } from './data.service';

@Injectable({
providedIn: 'root'
})
export class CustomerPriceService{
	url= this.rest.link_url();
	
	constructor(private rest: RestApiService,
				private data: DataService ) { }

	// dataLocalStorage.custNo = arr.No;
	// dataLocalStorage.custName = arr.Name;
	// dataLocalStorage.custName2 = arr.Name2;
	// dataLocalStorage.custCity = arr.City;
	// dataLocalStorage.custPhoneNo = arr.PhoneNo;
	// dataLocalStorage.custEmail = arr.Email;
	// dataLocalStorage.Top = arr.Top;
	// dataLocalStorage.discGroup = arr.DiscGroup;

	checkCustomerDiscGroup(arr){
		let dataLocalStorage :any={};
		try{
			if (JSON.parse(localStorage.getItem("order_draft")) === null){
				localStorage.setItem("order_draft",
					JSON.stringify({
						custNo : arr.No,
						custName :  arr.Name,
						custName2 : arr.Name2,
						custCity : arr.City,
						custPhoneNo : arr.PhoneNo,
						custEmail : arr.Email,
						Top : arr.Top,
						discGroup : arr.DiscGroup,
					})
				);  
			}else{
				let discGroup = JSON.parse(localStorage.getItem("order_draft")).discGroup;
				let item = JSON.parse(localStorage.getItem("order_draft")).item;
				let salesPerson = JSON.parse(localStorage.getItem("order_draft")).salesPerson;
				let notes = JSON.parse(localStorage.getItem("order_draft")).notes;
				let deliveryService = JSON.parse(localStorage.getItem("order_draft")).deliveryservice;
				let coupon = JSON.parse(localStorage.getItem("order_draft")).coupon; 
				localStorage.setItem("order_draft",
					JSON.stringify({
						custNo : arr.No,
						custName :  arr.Name,
						custName2 : arr.Name2,
						custCity : arr.City,
						custPhoneNo : arr.PhoneNo,
						custEmail : arr.Email,
						Top : arr.Top,
						discGroup : arr.DiscGroup,
						item : item,
						salesPerson: salesPerson,
						notes: notes,
						deliveryService: deliveryService,
						coupon: coupon
					})
				);  
				
				let dataItemCustomer = JSON.parse(localStorage.getItem("order_draft"));      
				// if (arr.DiscGroup !== discGroup){     
					if ((dataItemCustomer.item !== null) && typeof dataItemCustomer.item !== "undefined"){
						if (this.data.user.company === "SENTRALTUKANG"){
							dataItemCustomer.item.forEach(async (item, index)=>{
								let dataPriceSti = await this.rest.get(
									`${this.url}/api-item/cek-customer-price/${item.itemNo}/${arr.DiscGroup}`,
								);
								dataItemCustomer.item[index].price = dataPriceSti['item'][0].SalesPrice; 
								localStorage.setItem("order_draft",JSON.stringify(dataItemCustomer));  
							})
						// }
						// else if (this.data.user.company === "UNICODISTRIBUSI"){
						// 	localStorage.setItem("order_draft",JSON.stringify(dataItemCustomer));  
						// } else if (this.data.user.company === "KLASSEN") {
						// 	localStorage.setItem("order_draft", JSON.stringify(dataItemCustomer));
						}else{
							dataItemCustomer.item.forEach(async (item, index)=>{
								let dataPrice = await this.rest.get(
									// `${this.url}/api-item/cek-customer-price/${item.itemNo}/${arr.DiscGroup}`,
										`${this.url}/api-item/cek-customer-price/${item.itemNo}`,
								);
								dataItemCustomer.item[index].price = dataPrice['item'][0].SalesPrice; 
								localStorage.setItem("order_draft",JSON.stringify(dataItemCustomer));  
							})	
						}
					}
				// }
			} 
			
			return true;
		} catch(error){
			console.log(error);
			return false;
		}
	}
}
