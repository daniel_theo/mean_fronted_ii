import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

import { AuthGuardService } from './auth-guard.service';
import { SubmittedComponent } from './sales-order/order-product/submitted/submitted.component';

const routes: Routes = [
{
	// path : '',
	path : 'login',
  	loadChildren: './login/login.module#LoginModule'
},
{
	path : 'dashboard',
	canActivate: [AuthGuardService],
	loadChildren: './dashboard/dashboard.module#DashboardModule'
},
{
	path : 'accounts',
	canActivate: [AuthGuardService],
	loadChildren: './accounts/accounts.module#AccountsModule'
},
{
	path : 'settings',
	canActivate: [AuthGuardService],
	loadChildren: './settings/settings.module#SettingsModule'
},
{
	path : 'products',
	canActivate: [AuthGuardService],
	loadChildren: './product-catalog/product-catalog.module#ProductCatalogModule'
},
{
	path : 'sales-order',
	canActivate: [AuthGuardService],
	loadChildren: './sales-order/sales-order.module#SalesOrderModule'
},
{
	path : 'forgot-password',
	loadChildren: './forgot-password/forgot-password.module#ForgotPasswordModule'
},
{
	path : 'list-so',
	canActivate: [AuthGuardService],
	loadChildren: './list-so/list-so.module#ListSoModule'
},
{
	path: '**',
	redirectTo: 'dashboard'
	//redirectTo: 'dashboard',
	//pathMatch: 'full'
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
