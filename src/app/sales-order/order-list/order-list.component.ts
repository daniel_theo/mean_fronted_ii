import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';


@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  url = this.rest.link_url();
  salesOrder : Object;
  constructor(
    private rest: RestApiService,
    public data: DataService,
  ) {}


    // get data from localStorage order draft
    async ngOnInit() {
      try {
        const data = await this.rest.get(
          `${this.url}/api-sales-order/order`
        );
        
        data['success']
          ? (this.salesOrder = data['salesOrder'])
          : this.data.error(data['message']);
          // console.log(this.salesOrder);     
      } catch (error) {
        this.data.error(error['message']);
      }
    }


}
