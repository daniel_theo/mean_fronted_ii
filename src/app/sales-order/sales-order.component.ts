import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sales-order',
  template: `
      <router-outlet></router-outlet>
  `,
  styles: []
})
export class SalesOrderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
