import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialAppModule } from '../ngmaterial.module';

import { SalesOrderRoutingModule } from './sales-order-routing.module';
import { SalesOrderComponent } from './sales-order.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderProductComponent } from './order-product/order-product.component';

import { DialogCustomerComponent } from './order-product/dialog-customer/dialog-customer.component';
import { DialogSubmitComponent } from './order-product/dialog-submit/dialog-submit.component';
import { DialogPaymentComponent } from './order-product/dialog-payment/dialog-payment.component';
import { DialogDeleteOrderComponent } from './order-product/dialog-delete-order/dialog-delete-order.component';
import { DialogDraftComponent } from './order-product/dialog-draft/dialog-draft.component';
import { DialogDeliveryComponent } from './order-product/dialog-delivery/dialog-delivery.component';
import { DialogDeliveryAddComponent } from './order-product/dialog-delivery-add/dialog-delivery-add.component';
import { DialogDeliveryServiceComponent } from './order-product/dialog-delivery-service/dialog-delivery-service.component';
import { DialogCouponComponent } from './order-product/dialog-coupon/dialog-coupon.component';

import { SnackbarTrueComponent } from './snackbar-product/snackbar-true/snackbar-true.component';
import { SnackbarFalseComponent } from './snackbar-product/snackbar-false/snackbar-false.component';
import { DialogSalesComponent } from './order-product/dialog-sales/dialog-sales.component';
import { SubmittedComponent } from './order-product/submitted/submitted.component';
import { DialogDraftConfirmComponent } from './order-product/dialog-draft-confirm/dialog-draft-confirm.component';
import { ScrollingModule,ScrollDispatchModule  } from '@angular/cdk/scrolling';
import { DialogCustomerConfirmComponent } from './order-product/dialog-customer-confirm/dialog-customer-confirm.component';


@NgModule({
  imports: [
    CommonModule,
    SalesOrderRoutingModule,
    MaterialAppModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollingModule,
    ScrollDispatchModule
  ],
  entryComponents: [
    DialogCustomerComponent,
    DialogSubmitComponent,
    DialogPaymentComponent,
    DialogDeleteOrderComponent,
    DialogDraftComponent,
    DialogDeliveryComponent,
    DialogDeliveryServiceComponent,
    DialogCouponComponent,
    DialogSalesComponent,
    DialogDeliveryAddComponent,
    SnackbarTrueComponent,
    SnackbarFalseComponent,
    DialogDraftConfirmComponent,
    DialogCustomerConfirmComponent
  ],
  declarations: [
    SalesOrderComponent,
    OrderListComponent,
    OrderProductComponent,
    DialogCustomerComponent,
    DialogSubmitComponent,
    DialogPaymentComponent,
    DialogSalesComponent,
    DialogDeliveryAddComponent,
    DialogDeleteOrderComponent,
    DialogDraftComponent,
    DialogDeliveryComponent,
    DialogDeliveryServiceComponent,
    DialogCouponComponent,
    SnackbarTrueComponent,
    SnackbarFalseComponent,
    DialogSalesComponent,
    SubmittedComponent,
    DialogDraftConfirmComponent,
    DialogCustomerConfirmComponent
  ]
})
export class SalesOrderModule { }
