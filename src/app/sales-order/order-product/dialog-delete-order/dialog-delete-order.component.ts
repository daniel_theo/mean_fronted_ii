import { Component, OnInit } from '@angular/core';
import { SalesOrderService } from '../../../sales-order.service';

@Component({
  selector: 'app-dialog-delete-order',
  templateUrl: './dialog-delete-order.component.html',
  styleUrls: ['./dialog-delete-order.component.css']
})
export class DialogDeleteOrderComponent implements OnInit {
  constructor(
  	private sales_order : SalesOrderService
  ) { }

  async deleteOrder() {
    this.sales_order.deleteOrder();
  }

  ngOnInit() {
  }

}
