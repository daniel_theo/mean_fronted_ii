import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';

@Component({
    selector: 'app-dialog-coupon',
    templateUrl: './dialog-coupon.component.html',
    styleUrls: ['./dialog-coupon.component.css']
})
export class DialogCouponComponent implements OnInit {

    url= this.rest.link_url();
    coupon='';
    dataitemCoupon :any;
    dataAmount:any={};

    constructor(
        public dialogRef: MatDialogRef<DialogCouponComponent>,
        private data: DataService,
        private snackBar: MatSnackBar,
        private rest: RestApiService,
    ) { }

    async ngOnInit() {
    }

    async saveCoupon(){
        try {
            this.dataitemCoupon = JSON.parse(localStorage.getItem("order_draft")).item;
            this.dataitemCoupon.map(data=>data.coupon =this.coupon);
            
                const data = await this.rest.post(
                `${this.url}/api-getorder/coupon-promo`,
                this.dataitemCoupon);
                data['success']
                    ? (this.dataAmount   = data['amount'])
                    : this.data.error(data['message']);

                    if (this.dataAmount[0].number !== null){
                        localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),coupon: this.dataAmount[0].number,couponName:this.coupon}));
                    } if(this.dataAmount[0].number == 0){
                        localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),coupon: 0,couponName:''}));  
                        this.snackBar.open('Minimum order is not valid', 'OK', {
                            duration: 2000,
                        });
                    } if(this.dataAmount[0].number === null) {
                        localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),coupon: 0,couponName:''}));  
                        this.snackBar.open('Voucher cannot be found or expired', 'OK', {
                            duration: 2000,
                        });
                    }
                    this.dialogRef.close();

        } catch (error) {
            this.data.error(error['message']);
            this.dialogRef.close();
        }

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
