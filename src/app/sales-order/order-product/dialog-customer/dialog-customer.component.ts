import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ActivatedRoute, Router,Params } from '@angular/router';
import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { CustomerPriceService } from '../../../customer-price.service';

import { Subject, BehaviorSubject, Subscription, Observable, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, concatMap, map} from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { DialogCustomerConfirmComponent } from '../dialog-customer-confirm/dialog-customer-confirm.component';

@Component({
  selector: 'app-dialog-customer',
  templateUrl: './dialog-customer.component.html',
  styleUrls: ['./dialog-customer.component.css']
})

export class DialogCustomerComponent implements OnInit {
    @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
    @ViewChild('search') searchElement: ElementRef;
    customer:Object;
    dataLocalStorageBefore:any={};
    filter_description = "";

    dataItem:any ={};
    searchTerm$ = new Subject<string>();
    page = 1 ;
    Scrool = 1;
    
    countPage = 20 ;
    nextPage = 2 ;
    lastPage = 0 ;
    rangePage = 0 ;

    firstData = 0;
    LastData  = 0;
    GetRange  = 0;
    url= this.rest.link_url();

    filter_name_customer = "";
    filter="";
    filter_name="";
    public ds;
    @ViewChild('searchInput') input: ElementRef;
    // searchCust = {};
    private searchCust$ : Subscription;
    constructor(
      private data: DataService,
      public dialog: MatDialog,
      private custPrice: CustomerPriceService,
      private rest: RestApiService,
      private router: Router,
      private activatedRoute : ActivatedRoute,
      public dialogRef: MatDialogRef<DialogCustomerComponent>
    ) {
      this.data.getProfile;
      if (JSON.parse(localStorage.getItem("customer_list"))!== null && typeof JSON.parse(localStorage.getItem("customer_list"))!== undefined){
        this.filter_description = JSON.parse(localStorage.getItem("customer_list")).description;
      }
      this.page               = 1;
      
    }
    
    async ngOnInit() {
      this.customer = null;
      const dataNo = await this.rest.get(
        `${this.url}/api-customer/list-customer-scrolling-no?description=${this.filter_description}`
        );
        if(dataNo['success']){
            if (dataNo['customer'].length == 0){
                localStorage.setItem('customer_list', JSON.stringify({...JSON.parse(localStorage.getItem("customer_list")),description:this.filter_description,scrolling: 0}));
                this.Scrool = 0;
            } else {
                localStorage.setItem('customer_list', JSON.stringify({...JSON.parse(localStorage.getItem("customer_list")),description:this.filter_description,scrolling: dataNo['customer'][0].RowNumber}));
                this.Scrool = dataNo['customer'][0].RowNumber;
            }
              
            if (this.Scrool < 20){
                this.ds = null;
                let dataArrItem =[];
                const data = await this.rest.get(
                   // `${this.url}/api-customer/list-customer-pagination?page=1&description=${this.filter_description}`
                   `${this.url}/api-customer/list-customer-scrolling?page=1&description=${this.filter_description}` 
                 );
                if (data['success']){             
                    this.customer = data['customer'];
                }else{
                  this.data.error(data['message']);
                }     
            }else{
                this.ds = new MyDataSource(this.rest);
            }
        }else{
            this.data.error(dataNo['message']); 
        }
      try{
        this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
        await this.viewPort.scrollToIndex(0, 'smooth');
        await this.viewPort.scrollToIndex(6, 'smooth');
      }catch (error) {
      this.data.error(error['message']);

    }
  }
  ngAfterViewInit() {

        this.searchCust$  = fromEvent<any>(this.input.nativeElement, 'keyup')
            .pipe(
                    map(event => event.target.value),
                    debounceTime(400),
                    distinctUntilChanged(),
                    concatMap(search => this.ngOnInit())
            ).subscribe();
    }
    // customer dialog close
    onNoClick(): void {
      localStorage.removeItem('customer_list');
      this.dialogRef.close();
    }
    ClearSearch(){
      this.searchTerm$.next("");
      this.filter_description='';
    }
    ngOnDestroy(){
      localStorage.removeItem('customer_list');
      this.dialogRef.close();
      this.searchCust$.unsubscribe();
    }
  openDialogConfirm(arr) {
    const dialogRef = this.dialog.open(DialogCustomerConfirmComponent, {
      width: '800px',
      height: '180px',
      data: {
        arr
      },
    });
    // this.dialogRef.close();
    dialogRef.afterClosed().subscribe(arr => {
      // console.log(arr);
      
      // if (arr === true)
      //   window.location.reload();

    });
  }
    async processSearchCustomer(event: string) {   
        try{
            await this.ngOnInit();
        } catch (error) {
           this.data.error(error['message']);
        }   
    }
    // add customer to localStorage order draf
    async ordercustomer(arr) {
      try{
          let checkCust = this.custPrice.checkCustomerDiscGroup(arr);
          if (checkCust){
            this.dialogRef.close(); 
          } 
      }catch(error){
        console.log(error);
      }
  }
}
export class MyDataSource extends DataSource<string | undefined> {

  private length   = JSON.parse(localStorage.getItem("customer_list")).scrolling;
  private pageSize = 20;
  private cachedData = Array.from<string>({length: this.length});
  private fetchedPages = new Set<number>();
  private dataStream = new BehaviorSubject<(any | undefined)[]>(this.cachedData);
  private subscription = new Subscription();
 
  constructor(private rest: RestApiService) {
    super();
  }
  filter_description="";
  dataArr =[];
  connect(collectionViewer: CollectionViewer): Observable<(any | undefined)[]> {
    this.subscription.add(collectionViewer.viewChange.subscribe(range => {   
      const startPage = this.getPageForIndex(range.start);
      const endPage = this.getPageForIndex(range.end);
      for (let i = startPage; i <= endPage; i++) {
        this.fetchPage(i);
      }
    }));
    return this.dataStream;
  }

  disconnect(): void {
    this.subscription.unsubscribe();
  }

  private getPageForIndex(index: number): number {
    return Math.floor(index / this.pageSize);
  }

  private fetchPage(page: number) {
    if (this.fetchedPages.has(page)) {      
      return;
    }
    if (JSON.parse(localStorage.getItem("customer_list"))!== null && typeof JSON.parse(localStorage.getItem("customer_list"))!== undefined){
        this.filter_description = JSON.parse(localStorage.getItem("customer_list")).description;
      }
    this.fetchedPages.add(page);
    this.rest.getDataCustomer((page * this.pageSize)+1,this.filter_description ).subscribe(data => { 
      this.cachedData.splice(page * this.pageSize, this.pageSize,...data["customer"]);
      this.dataStream.next(this.cachedData);
    });
  }
}