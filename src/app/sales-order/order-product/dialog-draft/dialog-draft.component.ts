import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { SalesOrderService } from '../../../sales-order.service';
import { DialogDraftConfirmComponent } from '../dialog-draft-confirm/dialog-draft-confirm.component';

@Component({
  selector: 'app-dialog-draft',
  templateUrl: './dialog-draft.component.html',
  styleUrls: ['./dialog-draft.component.css']
})
export class DialogDraftComponent implements OnInit {
  dataItemDraft : any = {};
  dataDraftList = [];
  psn:any;

  constructor(
    public dialogRef: MatDialogRef<DialogDraftComponent>,
    private sales_order : SalesOrderService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("draft_List")) !== null){
      this.dataItemDraft =  JSON.parse(localStorage.getItem("draft_List"));
      this.dataDraftList = Object.values(this.dataItemDraft);
    }
  }
  openDialogDarftConfirm(arr,key): void {
    const dialogRef = this.dialog.open(DialogDraftConfirmComponent, {
      width: '600px',
      height: '130px',
      data: {
        arr,key        
      },
      autoFocus: false       
    });
    dialogRef.afterClosed().subscribe(arr => {
       window.location.reload();
    });
    this.onNoClick();
  }
  selectDraft(arr,key){
    this.sales_order.selectDraft(arr,Object.keys(this.dataItemDraft)[key]);
    this.psn = arr
    this.snackBar.open('Draft '+this.psn.custName, '', {
      duration: 1000,
    });
    this.dialogRef.close();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}