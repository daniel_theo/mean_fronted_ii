import { Component, OnInit, Inject } from '@angular/core';
import { SalesOrderService } from 'src/app/sales-order.service';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-draft-confirm',
  templateUrl: './dialog-draft-confirm.component.html',
  styleUrls: ['./dialog-draft-confirm.component.css']
})
export class DialogDraftConfirmComponent implements OnInit {
  dataItemDraft : any = {};
  dataDraftList = [];
  psn:any;
  constructor(
    @Inject(MAT_DIALOG_DATA) public dataitem: any,
    public dialogRef: MatDialogRef<DialogDraftConfirmComponent>,
    private sales_order : SalesOrderService,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("draft_List")) !== null){
      this.dataItemDraft =  JSON.parse(localStorage.getItem("draft_List"));
      this.dataDraftList = Object.values(this.dataItemDraft);
    }
  }

  selectDraft(){
    this.sales_order.selectDraft(this.dataitem.arr,Object.keys(this.dataItemDraft)[this.dataitem.key]);
    this.psn = this.dataitem.arr
    this.snackBar.open('Draft '+this.psn.custName, '', {
      duration: 1000,
    });
    this.dialogRef.close();
  }

}
