import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDraftConfirmComponent } from './dialog-draft-confirm.component';

describe('DialogDraftConfirmComponent', () => {
  let component: DialogDraftConfirmComponent;
  let fixture: ComponentFixture<DialogDraftConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogDraftConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDraftConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
