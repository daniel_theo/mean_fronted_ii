import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-delivery-service',
  templateUrl: './dialog-delivery-service.component.html',
  styleUrls: ['./dialog-delivery-service.component.css']
})
export class DialogDeliveryServiceComponent implements OnInit {
    deliveryCharge :number;
    constructor(
      public dialogRef: MatDialogRef<DialogDeliveryServiceComponent>,

    ) { }

    ngOnInit() {
    }

    saveDeliveryService(){
      localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),deliveryService: this.deliveryCharge})); 
      this.dialogRef.close();
    }

    onNoClick(): void {
      this.dialogRef.close();
    }
}
