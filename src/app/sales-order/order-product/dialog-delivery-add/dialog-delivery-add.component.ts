import { Component, OnInit, Inject  } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';

@Component({
  selector: 'app-dialog-delivery-add',
  templateUrl: './dialog-delivery-add.component.html',
  styleUrls: ['./dialog-delivery-add.component.css']
})
export class DialogDeliveryAddComponent implements OnInit {
    code = "" ;
    name = "";
    name2 = "";
    address = "";
    address2 = "";
    city= "";
    county= "";
    phone ="";
    url= this.rest.link_url();

    dataPostCode: any;
    dataCounty: any;

    constructor(
      public dialogRef: MatDialogRef<DialogDeliveryAddComponent>,
      @Inject(MAT_DIALOG_DATA) public dataCust: any,
      private rest: RestApiService,
      public data: DataService,
    ) { }

    ngOnInit() {
      this.AddDeliveryAddress();
    }

    async AddDeliveryAddress(){
      try {
          const dataPostCode= await this.rest.get(
            `${this.url}/api-customer/post-codes`
          );
          const dataCounty= await this.rest.get(
            `${this.url}/api-customer/county`
          );
         
          this.dataPostCode = dataPostCode['postCodes'];
          this.dataCounty = dataCounty['county'];

      } catch (error) {
          this.data.error(error['message']);
      }
    }

    async saveDeliveryAddress(){
      let splitCity = this.city.split("=");
      let codeAddress = this.code.toUpperCase();
        try {
            const data =  await this.rest.post(
              `${this.url}/api-customer/add-delivery-address`,{
                  custNo:this.dataCust.custNo,
                  code: codeAddress,
                  name: this.name,
                  address: this.address,
                  address2: this.address2,
                  phone: this.phone,
                  city: splitCity[1],
                  postCode: splitCity[0],
                  county: this.county
              }
            );  

          } catch (error) {
            console.log(error);
            this.data.error(error['message']);
        }
      this.dialogRef.close();
    }

    onNoClick(): void {
      this.dialogRef.close();
    }
}
