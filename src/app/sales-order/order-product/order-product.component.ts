import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatStepper } from '@angular/material';
import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';
import { SalesOrderService } from '../../sales-order.service';

import {Location} from '@angular/common';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DialogCustomerComponent } from './dialog-customer/dialog-customer.component';
import { DialogSalesComponent } from './dialog-sales/dialog-sales.component';
import { DialogPaymentComponent } from './dialog-payment/dialog-payment.component';
import { DialogSubmitComponent } from './dialog-submit/dialog-submit.component';
import { DialogDeleteOrderComponent } from './dialog-delete-order/dialog-delete-order.component';
import { DialogDraftComponent } from './dialog-draft/dialog-draft.component';
import { DialogDeliveryComponent } from './dialog-delivery/dialog-delivery.component';
import { DialogDeliveryServiceComponent } from './dialog-delivery-service/dialog-delivery-service.component';
import { DialogCouponComponent } from './dialog-coupon/dialog-coupon.component';

import { MatSnackBar } from '@angular/material';
import { SnackbarTrueComponent } from '../snackbar-product/snackbar-true/snackbar-true.component';
import { SnackbarFalseComponent } from '../snackbar-product/snackbar-false/snackbar-false.component';

@Component({
  selector: 'app-order-product',
  templateUrl: './order-product.component.html',
  styleUrls: ['./order-product.component.css']
})
export class OrderProductComponent implements OnInit {
    show: boolean = false;
    isLinear =false;
    pageIndex = 0;
    firstFormGroup: FormGroup;  
    secondFormGroup: FormGroup; 
    thirdFormGroup: FormGroup;
    dataItem : any = {};
    dataItemPayment : any = {};
    dataSave : any = {};
    dataPost : any = {};
    cartItems = 0;
    total = 0;
    notes = '';
    paymentMethod ='';
    coupon = 0;
    dataPayment: any;
    url = this.rest.link_url();
    dataItemDraft : any = {};
    dataDraftList = [];
    desc = "";
    btnSubmitted = false;
    @ViewChild('stepper') stepper: MatStepper;
    constructor(
      private _formBuilder: FormBuilder,
      public dialog: MatDialog,
      private _location: Location,
      public data: DataService,
      private rest: RestApiService,
      private sales_order : SalesOrderService,
      public snackBar: MatSnackBar,
    ) {}

    back() {
      this._location.back();
    }

    moveAfterReload() {
      this.pageIndex =  JSON.parse(localStorage.getItem("stepper"));
      if(this.pageIndex != undefined){
        this.stepper.selectedIndex = 1;
        this.isLinear =true;
      }else{
        this.stepper.selectedIndex = 0;
        this.isLinear =false;
      }
    }
    async ngOnInit() {
      this.moveAfterReload()
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
      if (this.dataItem === null){
          this.notes="";
          this.coupon = 0;
          this.paymentMethod =""
      }else{
          this.notes= this.dataItem.notes;
          this.coupon  =this.dataItem.coupon;
          this.paymentMethod = this.dataItem.paymentMethod;
      }
      this.firstFormGroup = this._formBuilder.group({
        firstCtrl: ['']
      });
      this.secondFormGroup = this._formBuilder.group({
        secondCtrl: ['']
      });
      
      this.dataItemDraft =  JSON.parse(localStorage.getItem("draft_List")); 
      try{
          const dataPaymentMethod = await this.rest.get(
            `${this.url}/api-getorder/payment-method`
          );
              this.dataPayment = dataPaymentMethod['payment'];
              this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
              this.cartTotal();
      }catch(error){
        console.log(error.message);
      }
    }

    ngOnDestroy() {
      localStorage.removeItem('stepper');
    }

    async changeQty(arr){
      try{
        await this.sales_order.itemLocalStorage(arr);
        this.cartTotal();
      }catch(error){
        console.log(error.message);
      }
    }

    inputNotes(newValue) {
      localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),notes: this.notes}));     
    }

    onChange(newValue) {
      localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),paymentMethod: this.paymentMethod}));     
    }
   

    cartTotal(){
      try{
          this.total = this.sales_order.countItemAmountChart();
          return this.total;
       }catch(error){
          console.log(error.message);
      }
    }

    // remove product and update localStorage
    async removeFromCart (arr){
      try{
        await this.sales_order.removeChart(arr);
        localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),coupon: 0,couponName:''}));
        this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
        this.cartTotal();
       }catch(error){
        console.log(error.message);
      }
    }

    async saveDraft(){
      try{
        let ObjOrderDraft = 0;
        if(this.dataItem === null){
          ObjOrderDraft = 0;
        }else{
          ObjOrderDraft = 1;
        }
        if(ObjOrderDraft >0 ){
            await this.sales_order.saveDraft();
            this.cartTotal();
            this.dataItem = null;
            this.notes ="";
            this.snackBar.openFromComponent(SnackbarTrueComponent, {
                  duration: 1000,
              });
              this.dataItemDraft =  JSON.parse(localStorage.getItem("draft_List"));
              this.dataDraftList = Object.values(this.dataItemDraft); 
          }else {
            this.snackBar.openFromComponent(SnackbarFalseComponent, {
                  duration: 1000,
              });
          }
      }catch(error){
        console.log(error.message);
      }
    }
    async saveSalesOrder(){
      this.dataSave = await JSON.parse(localStorage.getItem("order_draft")); 
      
        this.dataPost = await Object.assign({}, this.dataSave);
        try {
            const data =  await this.rest.post(
              `${this.url}/api-sales-order/sales-order`,this.dataPost
            );  

              if (data['success']) {
                this.dataItem = null;
                this.dataPayment = null;
                this.total =0;
                this.notes ="";
                this.paymentMethod ="";
                this.sales_order.deleteOrder();
              }
           
          } catch (error) {
            console.log(error);
            this.data.error(error['message']);
        }
  }
    
  openDialogCustomer(): void {
    const dialogRef = this.dialog.open(DialogCustomerComponent, {
      height: '500px',
      width: '800px'
    });
    //get data from localStorage order draft when selected in dialog customer
    // dialogRef.afterClosed().subscribe(arr => {
    //    window.location.reload();
    // });
  }
    openDialogSales(): void {
    const dialogRef = this.dialog.open(DialogSalesComponent, {
      height: 'auto',
      width: '800px'
    });
    dialogRef.afterClosed().subscribe(arr => {
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
    });
  }
    // open dialog delete order
    openDialogDelete(): void {
    const dialogRef = this.dialog.open(DialogDeleteOrderComponent, {
      width: '800px',
      height: '130px'
    });

    dialogRef.afterClosed().subscribe(arr => {
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
      this.cartTotal();
    });
  }
    openDialogSubmit(items): void {
    const dialogRef = this.dialog.open(DialogSubmitComponent, {
      width: '800px',
      height: '130px',
      data: {
        items
      },
      autoFocus: false 
    });

    dialogRef.afterClosed().subscribe(arr => {
      if (!this.btnSubmitted){
        (arr)
        ?this.btnSubmitted=true
        :this.btnSubmitted=false;
      }
      setTimeout(() => {
        this.btnSubmitted = false
      }, 5000);
    });
  }

    // open dialog payment
    openDialogPayment(): void {
    const dialogRef = this.dialog.open(DialogPaymentComponent, {
      height: '600px',
      width: '800px'
    });

    // get data from localStorage order draft when selected in dialog payment
    dialogRef.afterClosed().subscribe(arr => {
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
    });
  }


    // open dialog draft
    openDialogDraft(): void {
    const dialogRef = this.dialog.open(DialogDraftComponent, {
      width: '800px'
    });

    dialogRef.afterClosed().subscribe(arr => {
      
    });
  }

    // open dialog delivery
    openDialogDelivery(custNo): void {
    const dialogRef = this.dialog.open(DialogDeliveryComponent, {
      width: '800px',
      data: {
        custNo
      },
    });
    dialogRef.afterClosed().subscribe(arr => {
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
    });
  }

    // open dialog delivery service
    openDialogDeliveryService(): void {
    const dialogRef = this.dialog.open(DialogDeliveryServiceComponent, {
      width: '800px',
      height: '240px'
    });
    
        // get data from localStorage order draft when selected in dialog payment
    dialogRef.afterClosed().subscribe(arr => {
      this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
      this.cartTotal();
    });
  }

  // open dialog coupon
  openDialogCoupon(): void {
    const dialogRef = this.dialog.open(DialogCouponComponent, {
      width: '800px',
      height: '220px'
    });
    dialogRef.afterClosed().subscribe(arr => {
        localStorage.setItem('stepper', '1');
        this.ngOnInit();this.cartTotal();
        localStorage.removeItem('stepper');
    });
  }

  viewPayment(arr){
    this.dataItemPayment = arr
    localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),paymentMethod: arr}));   
  }

}
