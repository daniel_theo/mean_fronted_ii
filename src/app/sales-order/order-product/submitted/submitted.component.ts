import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { SalesOrderService } from 'src/app/sales-order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-submitted',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.css']
})
export class SubmittedComponent implements OnInit {
  dataItem : any = {};
  total = 0;
  constructor(
    public data: DataService,
    private sales_order : SalesOrderService,
    private router: Router  
  ) { }

  ngOnInit() {
    this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
    localStorage.removeItem('stepper');
    this.cartTotal();
  }
  ngOnDestroy() {
    this.sales_order.deleteOrder();
  }
  back(){
    this.router.navigate(['sales-order/']);
  }
  cartTotal(){
    try{
      // if (this.data.user.company === "UNICODISTRIBUSI"){
      //   this.total = this.sales_order.countItemAmountChartUSD();
      // }else{
        this.total = this.sales_order.countItemAmountChart();
      // }
      return this.total;
     }catch(error){
      console.log(error.message);
    }
  }

}
