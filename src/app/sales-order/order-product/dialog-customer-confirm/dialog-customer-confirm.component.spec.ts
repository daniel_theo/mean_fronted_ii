import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCustomerConfirmComponent } from './dialog-customer-confirm.component';

describe('DialogCustomerConfirmComponent', () => {
  let component: DialogCustomerConfirmComponent;
  let fixture: ComponentFixture<DialogCustomerConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCustomerConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCustomerConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
