import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogDraftConfirmComponent } from '../dialog-draft-confirm/dialog-draft-confirm.component';
import { CustomerPriceService } from 'src/app/customer-price.service';
import { DataService } from 'src/app/data.service';
import { RestApiService } from 'src/app/rest-api.service';

@Component({
  selector: 'app-dialog-customer-confirm',
  templateUrl: './dialog-customer-confirm.component.html',
  styleUrls: ['./dialog-customer-confirm.component.css']
})
export class DialogCustomerConfirmComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public dataitem: any,
    public dialogRef: MatDialogRef<DialogDraftConfirmComponent>,
    public snackBar: MatSnackBar,    
    private custPrice: CustomerPriceService,
    public dialog: MatDialog,
    private rest: RestApiService,
    private data: DataService
  ) { }

  ngOnInit() {
  }
    async ordercustomer() {
        // let statusApi = false;
        try {
            if (JSON.parse(localStorage.getItem("order_draft")) === null) {
                localStorage.setItem("order_draft",
                JSON.stringify({
                    custNo: this.dataitem.arr.No,
                    custName: this.dataitem.arr.Name,
                    custName2: this.dataitem.arr.Name2,
                    custCity: this.dataitem.arr.City,
                    custPhoneNo: this.dataitem.arr.PhoneNo,
                    custEmail: this.dataitem.arr.Email,
                    Top: this.dataitem.arr.Top,
                    discGroup: this.dataitem.arr.DiscGroup,
                })
                );
            } else {
                let discGroup = JSON.parse(localStorage.getItem("order_draft")).discGroup;
                let item = JSON.parse(localStorage.getItem("order_draft")).item;
                let salesPerson = JSON.parse(localStorage.getItem("order_draft")).salesPerson;
                let notes = JSON.parse(localStorage.getItem("order_draft")).notes;
                let deliveryService = JSON.parse(localStorage.getItem("order_draft")).deliveryservice;
                let coupon = JSON.parse(localStorage.getItem("order_draft")).coupon;
                localStorage.setItem("order_draft",
                    JSON.stringify({
                    custNo: this.dataitem.arr.No,
                    custName: this.dataitem.arr.Name,
                    custName2: this.dataitem.arr.Name2,
                    custCity: this.dataitem.arr.City,
                    custPhoneNo: this.dataitem.arr.PhoneNo,
                    custEmail: this.dataitem.arr.Email,
                    Top: this.dataitem.arr.Top,
                    discGroup: this.dataitem.arr.DiscGroup,
                    item: item,
                    salesPerson: salesPerson,
                    notes: notes,
                    deliveryService: deliveryService,
                    coupon: coupon
                    })
                );


                let dataItemCustomer = await JSON.parse(localStorage.getItem("order_draft"));
            //   if (this.dataitem.arr.DiscGroup !== discGroup) {
                if ((dataItemCustomer.item !== null) && typeof dataItemCustomer.item !== "undefined") {
                    if (this.data.user.company === "SENTRALTUKANG") {
                        dataItemCustomer.item.forEach(async (item, index) => {
                        this.rest.getCustomerPriceSti(item.itemNo, this.dataitem.arr.DiscGroup).subscribe((data) => {
                            dataItemCustomer.item[index].price = data['item'][0].SalesPrice;
                            localStorage.setItem("order_draft", JSON.stringify(dataItemCustomer));
                            if (index === (dataItemCustomer.item.length - 1)) {
                            localStorage.setItem('stepper', '1');
                            this.dialogRef.close();
                            window.location.reload();
                            }

                        })
                        })
                    //   } else if (this.data.user.company === "UNICODISTRIBUSI") {
                    //     localStorage.setItem("order_draft", JSON.stringify(dataItemCustomer));
                    //   } else if (this.data.user.company === "KLASSEN") {
                    //     localStorage.setItem("order_draft", JSON.stringify(dataItemCustomer));
                    } else {
                        dataItemCustomer.item.forEach(async (item, index) => {
                            this.rest.getCustomerPrice(item.itemNo, this.dataitem.arr.DiscGroup).subscribe((data) => {
                                
                                (data['item'].length > 0)
                                    ?dataItemCustomer.item[index].price = data['item'][0].SalesPrice
                                    :dataItemCustomer.item[index].price = 0;

                                localStorage.setItem("order_draft", JSON.stringify(dataItemCustomer));
                                if (index === (dataItemCustomer.item.length - 1)) {
                                    localStorage.setItem('stepper', '1');
                                    this.dialogRef.close();
                                    window.location.reload();
                                }
                            })
                        })
                    }
                // }
                } else {
                    localStorage.setItem('stepper', '1');
                    this.dialogRef.close();
                    window.location.reload();
                }
            }
        } catch (error) {
        console.log(error);
        return false;
        }
    }
}
