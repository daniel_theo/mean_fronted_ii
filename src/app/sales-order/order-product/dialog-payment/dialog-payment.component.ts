import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';

@Component({
  selector: 'app-dialog-payment',
  templateUrl: './dialog-payment.component.html',
  styleUrls: ['../order-product.component.css']
})
export class DialogPaymentComponent implements OnInit {
  dataLocalStorageBefore:any={};
  toptop:Object;
  url= this.rest.link_url();

  constructor(
    private data: DataService,
    private rest: RestApiService,
    public dialogRef: MatDialogRef<DialogPaymentComponent>
  ) {}

  async ngOnInit() {
      const data = await this.rest.get(
        `${this.url}/api-customer/top`
      );
      this.toptop = data['item'];
    } catch (error) {
      this.data.error(error['message']);
  }

  // dialog payment close
  onNoClick(): void {
    this.dialogRef.close();
  }

  // add TOP to localStorage order draf
    async addTOP(arr) {
       if (JSON.parse(localStorage.getItem("order_draft")) === null){
        this.dataLocalStorageBefore.Top = arr.Code;
        localStorage.setItem("order_draft",JSON.stringify(this.dataLocalStorageBefore));
        this.dialogRef.close();
       }else{
        this.dataLocalStorageBefore   = JSON.parse(localStorage.getItem("order_draft"));
        this.dataLocalStorageBefore.Top = arr.Code;
        localStorage.setItem("order_draft",JSON.stringify(this.dataLocalStorageBefore));
        this.dialogRef.close();
      }
  }
}
