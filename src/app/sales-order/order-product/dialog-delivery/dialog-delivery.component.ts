import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA,MatDialog } from '@angular/material';
import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { DialogDeliveryAddComponent } from '../dialog-delivery-add/dialog-delivery-add.component';

@Component({
  selector: 'app-dialog-delivery',
  templateUrl: './dialog-delivery.component.html',
  styleUrls: ['./dialog-delivery.component.css']
})
export class DialogDeliveryComponent implements OnInit {

    url= this.rest.link_url();
    deliveryAddress:Object;
    dataDelivery:any;
    dataItem:any;

    constructor(
      public dialogRef: MatDialogRef<DialogDeliveryComponent>,
      @Inject(MAT_DIALOG_DATA) public dataCust: any,
      private data: DataService,
      private rest: RestApiService,
      public dialog: MatDialog
    ) { }

    async ngOnInit() {
      if (this.dataCust.custNo !== undefined){
        this.dataItem =  JSON.parse(localStorage.getItem("order_draft"));
        try {
                const data = await this.rest.get(
               `${this.url}/api-customer/delivery-address/${this.dataCust.custNo}`
             );
            data['success']
               ? (this.deliveryAddress = data['address'])
               : this.data.error(data['message']);
               
        } catch (error) {
           this.data.error(error['message']);
        }
      }
    }

    saveDeliveryAddress(arr){
      localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),deliveryAddress: arr})); 
      this.dialogRef.close();
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    addDeliveryAddress(): void {
      let custNo = this.dataCust.custNo;
      const dialogRef = this.dialog.open(DialogDeliveryAddComponent, {
        width: '600px',
        height: '460px',
        data: {
          custNo
        },
      });
      dialogRef.afterClosed().subscribe(arr => {
       this.ngOnInit();
      });
    }

}
