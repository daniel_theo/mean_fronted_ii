import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router,Params } from '@angular/router';
import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';

import {Subject} from 'rxjs';
import { CustomerPriceService } from 'src/app/customer-price.service';

@Component({
  selector: 'app-dialog-sales',
  templateUrl: './dialog-sales.component.html',
  styleUrls: ['./dialog-sales.component.css']
})
export class DialogSalesComponent implements OnInit {
  @ViewChild('search') searchElement: ElementRef;
  salesPerson :any =[];
  dataLocalStorageBefore:any={};

  searchTerm$ = new Subject<string>();
  page = 1 ;

  countPage = 20 ;
  nextPage = 2 ;
  lastPage = 0 ;
  rangePage = 0 ;

  firstData = 0;
  LastData  = 0;
  GetRange  = 0;

  url= this.rest.link_url();

  filter_name_sales = "";
  filter="";
  filter_name="";

  constructor(    
    private data: DataService,
    private rest: RestApiService,
    public dialogRef: MatDialogRef<DialogSalesComponent>
  ) { }

  async ngOnInit() {    
    try {
      const dataEx = await this.rest.get(
        `${this.url}/api-customer/salesPerson`
      );
      this.salesPerson = dataEx['salesCode'];
  } catch (error) {
      this.data.error(error['message']);
  }
  } 

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  async orderSales(arr) {
    localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),salesPerson: arr})); 
    this.dialogRef.close(); 
    }
}
