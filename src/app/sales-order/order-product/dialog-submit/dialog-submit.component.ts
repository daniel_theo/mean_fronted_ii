import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { SalesOrderService } from '../../../sales-order.service';
import { Router} from '@angular/router'; 
import { DataService } from 'src/app/data.service';
import { RestApiService } from 'src/app/rest-api.service';

@Component({
  selector: 'app-dialog-submit',
  templateUrl: './dialog-submit.component.html',
  styleUrls: ['../order-product.component.css']
})
export class DialogSubmitComponent implements OnInit {
  total = 0 ;
  dataSave : any = {};
  dataPost : any = {};
  url = this.rest.link_url();
  constructor(
    @Inject(MAT_DIALOG_DATA) public dataitem: any,
    public dialogRef: MatDialogRef<DialogSubmitComponent>,
    private sales_order : SalesOrderService,
    private rest: RestApiService,
    public data: DataService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {    
  }

  async saveDraft(){
    await this.sales_order.saveDraft();
    this.cartTotal();
  }
  async saveSalesOrder(){
    this.dataSave = await JSON.parse(localStorage.getItem("order_draft"));
      var i = 0
      var ii = 0
      if (this.data.user.company === "UNICODISTRIBUSI"){
          this.dataSave.totalAmount = this.sales_order.countItemAmountChartUSD();
        }else{
          this.dataSave.totalAmount  = this.sales_order.countItemAmountChart();
        }
      this.dataPost = await Object.assign({}, this.dataSave);
      try {
          const data =  await this.rest.post(
            `${this.url}/api-getorder/sales-order`,this.dataPost
          );
            if (data['success']) {
              localStorage.setItem("order_draft",JSON.stringify({...JSON.parse(localStorage.getItem("order_draft")),soNumber: data['soNumber']})); 
              this.router.navigate(['sales-order/submitted/']);
            }
      } catch (error) {
        this._snackBar.open('Saving Order Failed', '', {
          duration: 5000,
        });
        this.data.error(error['message']);
      }
  }

  cartTotal(){
    this.total = this.sales_order.countItemAmountChart();
    return this.total;
  }

}
