import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalesOrderComponent } from './sales-order.component';
import { OrderProductComponent } from './order-product/order-product.component';
import { OrderListComponent } from './order-list/order-list.component';
import { SubmittedComponent } from './order-product/submitted/submitted.component';

const routes: Routes = [
{
	path: '',
    component: SalesOrderComponent,
     children: [
      {
        path: '',
        component: OrderProductComponent
        
      },
      {
        path: 'order-list',
        component: OrderListComponent
      },
      {
        path: 'submitted',
        component: SubmittedComponent
      }
    ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesOrderRoutingModule { }
