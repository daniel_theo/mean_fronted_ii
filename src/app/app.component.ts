import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy,  OnInit} from '@angular/core';

import { Router } from '@angular/router';
import { DataService } from './data.service';
import { SalesOrderService } from './sales-order.service';
// import { MatSnackBar } from '@angular/material';
// import { ServiceWorkerModule , SwUpdate, SwPush} from '@angular/service-worker';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  navLinks = [
    { path: '/dashboard/', label: 'Home' },
    // { path: '/products/list', label: 'Home' },
    { path: '/list-so/list', label: 'Order' },
  ];

  title = 'app';
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public router: Router,
    public data: DataService,
    private sales_order:SalesOrderService,
    // public snackBar: MatSnackBar,
    // private swUpdate: SwUpdate
  ) {
    this.matIconRegistry.addSvgIcon(
      "home",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/menu-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "assignment-black",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/assignment-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "back",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/keyboard_arrow_left-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "search",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/search-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "close",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/close-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "more",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/more_horiz-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "delete_outline",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/delete_outline-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "bookmark_outline",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/bookmarks-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "save",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/save-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "save-outline",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/save-outline.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "add",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/add-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "more_vert",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/more_vert-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "help",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/help-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "autorenew",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/autorenew-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "delete", 
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/delete.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "update",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/update-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "visibility",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/visibility-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "visibility_off",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/visibility_off-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "colorize",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/colorize-24px.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "assignment",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/svg/assignment-outline-24px.svg")
    );
    
    this.data.getProfile();
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.sales_order.chartQty;
    // this.swUpdate.available.subscribe(event => {
    //    const snack = snackBar.open("A new version of this app is available ","Update");
    //    snack.onAction().subscribe(()=>{
    //      window.location.reload();
    //    })
    // })
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit(){
  }

  // logout
  logout() {
    localStorage.clear();
    window.location.replace('/login');
  }

// get token from localStorage
   get token() {
     return localStorage.getItem('token');
   }

   isDetailRoute(value: string): boolean {
     return /^(?!.*detail)/.test(value);    // Apabila *detail* maka Hiden, selain itu Tampil
   }
   isOrderRoute(value: string): boolean {
     return /^(?!.*sales-order)/.test(value);    // Apabila *order* maka Hiden, selain itu Tampil
   }

   isAccountsRoute(value: string): boolean {
     return /^(?!.*accounts)/.test(value);    // Apabila *accounts* maka Hiden, selain itu Tampil
   }

   isSettingsRoute(value: string): boolean {
     return /^(?!.*settings)/.test(value);    // Apabila *settings* maka Hiden, selain itu Tampil
   }
   
   isProductsRoute(value: string): boolean {
    return /^(?!.*products)/.test(value);    // Apabila *products* maka Hiden, selain itu Tampil
  }


}
