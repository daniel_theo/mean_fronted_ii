import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/rest-api.service';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  itemId: any;
  url= this.rest.link_url();
  dataItemHeader: any = {};
  dataItemComment: any = {};
  dataItemLine: any = {};
  dataItemDeliveryAddress: any={};
  dataItemDeliveryService: any={};
  dataItemPromo: any={};
  dataItemRounding: any = {};  
  dataItem: any={};
  dataItemBankCharge: any = {};
  dataItems: object;
  id: any;
  totalItem = 0;
  totalPromo = 0;
  Rounding = 0;
  totalDelivery = 0;
  totalBankCharge = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private rest: RestApiService,
    public data: DataService
  ) {
    this.id = activatedRoute.snapshot.url[1].path;
    this.data.getProfile();
    }

  async ngOnInit() {
    const id = this.id;
    this.dataHeader(id);
    // this.dataComment(id);
    // this.dataDeliveryService(id);
    // this.dataPromo(id);
    // this.dataItem(id);
  }
  async dataHeader(id){
    const data = await this.rest.get(
      `${this.url}/api-getorder/order-detail-header/${id}`
    );
    this.dataItemHeader = data['header'][0];
    this.dataItemComment = data['comment'][0]; 
    this.dataItem = data['item'];
    this.dataItemDeliveryService = data['delivery'][0];
    if (this.dataItemDeliveryService !== undefined) {
      this.totalDelivery = this.dataItemDeliveryService.deliveryPrice;
    }
    this.dataItemPromo = data['coupon'][0];
    if (this.dataItemPromo !== undefined) {
      this.totalPromo = this.dataItemPromo.itemPrice;
    }
    
    (data['rounding'][0] !== undefined)
      ? this.Rounding = data['rounding'][0].itemPrice
      :this.Rounding = 0;

    this.dataItemBankCharge = data['bankcharge'][0];
    if (this.dataItemBankCharge !== undefined) {
      this.totalBankCharge = this.dataItemBankCharge['price'];
    }

    this.totalItem = this.dataItem.reduce((acc, cur) => acc + (cur.itemQty * cur.itemPrice), 0);

    data['success']
      ? (this.dataItems = data['item'])
      : this.data.error(data['message']);
  
    // console.log(this.dataItemHeader.paymentMethod);
  }
  // async dataComment(id){
  //   const data = await this.rest.get(
  //     `${this.url}/api-getorder/order-detail-comment/${id}`
  //   );
    
  //   // console.log(this.dataItemComment.Comment);
  // }  
  // async dataDeliveryService(id){
  //   const data = await this.rest.get(
  //     `${this.url}/api-getorder/order-detail-delivery-service/${id}`
  //   );
  //   this.dataItemDeliveryService = data['delivery'][0];
  //   if (this.dataItemDeliveryService !== undefined){
  //     this.totalDelivery = this.dataItemDeliveryService.deliveryPrice;
  //   }
  //   // console.log(this.dataItemDeliveryService.deliveryPrice);
  // }
  // async dataPromo(id){
  //   const data = await this.rest.get(
  //     `${this.url}/api-getorder/order-detail-coupon/${id}`
  //   );
  //   this.dataItemPromo = data['coupon'][0];
  //   if (this.dataItemPromo !== undefined){
  //     this.totalPromo = this.dataItemPromo.itemPrice;
  //   }
  //   // console.log(this.dataItemPromo);
  // }
  // async dataItem(id){
  //   const data = await this.rest.get(
  //     `${this.url}/api-getorder/order-detail-line/${id}`
  //   );
    
}
