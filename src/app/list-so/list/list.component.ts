import { Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';

import { Router,ActivatedRoute ,Params } from '@angular/router';
import { Subject, BehaviorSubject, Subscription, Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild(CdkVirtualScrollViewport) viewPort: CdkVirtualScrollViewport;
  url = this.rest.link_url();
  salesOrder : Object;
  searchTerm$ = new Subject<string>();

  filter_description = "";
  filter = "";  
  filters = "";  
  Scrool = 1;

  weekStart = '';
  weekEnd = '';
  currDate = new Date;
  currDate2 = new Date;
  public ds;

  constructor(
    private rest: RestApiService,
    public data: DataService,
    private router: Router,
    private activatedRoute : ActivatedRoute,
  ) {
      this.data.getProfile;
      if (JSON.parse(localStorage.getItem("so_list"))!== null && typeof JSON.parse(localStorage.getItem("so_list"))!== undefined){
        this.filter_description = JSON.parse(localStorage.getItem("so_list")).description;
      }
      let first = this.currDate.getDate() - this.currDate.getDay();
      this.weekStart = new Date(this.currDate.setDate(first)).toISOString().split('T')[0];
      this.weekEnd = new Date(this.currDate2.setDate(first + 6)).toISOString().split('T')[0];
  }
  async ngOnInit() {
    this.salesOrder   = null;
    // await this.viewPort.scrollToIndex(0, 'smooth');
      // if(typeof(this.filter_description)== "undefined"){
      //     this.filter= `startingDate=${this.weekStart}&endingDate=${this.weekEnd}`;
      // }else{
      //     this.filter= `startingDate=${this.weekStart}&endingDate=${this.weekEnd}&name=${this.filter_description}`;
      // }

      // if (JSON.parse(localStorage.getItem("so_list"))!== null && typeof JSON.parse(localStorage.getItem("so_list"))!== undefined){
      if(this.filter_description === ""){ 
        this.filter =  this.filter= `startingDate=${this.weekStart}&endingDate=${this.weekEnd}`;
      }else{
        
        if(this.filter_description === undefined){
              this.filter= `startingDate=${this.weekStart}&endingDate=${this.weekEnd}`;
          }else{
              this.filter= `startingDate=${this.weekStart}&endingDate=${this.weekEnd}&name=${this.filter_description}`;
          }
      }
      try {
          const data = await this.rest.get(
          `${this.url}/api-getorder/order-no?${this.filter}`
        );
        if(data['success']){
          if (data['so'].length == 0){
            localStorage.setItem('so_list', JSON.stringify({...JSON.parse(localStorage.getItem("so_list")),description:this.filter_description,scrolling: 0,startingDate:this.weekStart,endingDate:this.weekEnd}));
            this.Scrool = 0;
            this.filters = this.filter_description;
          } else {
          localStorage.setItem('so_list', JSON.stringify({...JSON.parse(localStorage.getItem("so_list")),description:this.filter_description,scrolling: data['so'][0].RowNumber,startingDate:this.weekStart,endingDate:this.weekEnd}));
          this.Scrool = data['so'][0].RowNumber;
          }
          if (this.Scrool <= 20){
              const data = await this.rest.get(
                `${this.url}/api-getorder/order-scrolling?page=1&startingDate=${this.weekStart}&endingDate=${this.weekEnd  }&description=${this.filter_description}`
              );
              if (data['success']){
                  this.salesOrder   = data['salesOrder'];
              }else{
                this.data.error(data['message']);
              }     
          }else{
              await this.viewPort.scrollToIndex(0, 'smooth');
              await this.viewPort.scrollToIndex(6, 'smooth');
              // this.ds = new MyDataSource(this.rest);
              this.ds = new MyDataSource(this.rest, this.Scrool, this.filter_description);
          }
        }
    } catch (error) {
        this.data.error(error['message']);
      }
   };

   async searchSo(event: any){
       this.weekStart = new Date('2015-01-01').toISOString().split('T')[0];
       // tes
        // console.log(this.filter,this.weekEnd,this.filter_description);
        this.filter= `startingDate=${this.weekStart}&endingDate=${this.weekEnd}&name=${this.filter_description}`;
        try{
          // const dataNo = await this.rest.get(
          //         `${this.url}/api-getorder/order-no?${this.filter}`
          //    );
          // if(dataNo['success']){
          //  localStorage.setItem('so_list', JSON.stringify({description:this.filter_description,scrolling: dataNo['so'][0].RowNumber,startingDate:this.weekStart,endingDate:this.weekEnd}));
           // this.ds = new MyDataSource(this.rest);
           await this.ngOnInit();
           // }else{
           //     this.data.error(dataNo['message']); 
           // }
           } catch (error) {
              this.data.error(error['message']);
           }
   }
   getListSO(id){
      this.router.navigate(['/list-so/detail/',id]);
   }

   // clear search
  ClearSearch(){
    this.searchTerm$.next("");
    this.filter_description='';
  }

  ngOnDestroy() {
    localStorage.removeItem('so_list');
    this.searchTerm$.unsubscribe();
  }


}


export class MyDataSource extends DataSource<string | undefined> {

  private length   = this.scrollParam;
  private pageSize = 20;
  private cachedData = Array.from<string>({length: this.length});
  private fetchedPages = new Set<number>();
  private dataStream = new BehaviorSubject<(any | undefined)[]>(this.cachedData);
  private subscription = new Subscription();

  constructor(
    private rest: RestApiService,
    private scrollParam,
    private descriptionParam) {
    super();
  }
  filter_description="";
  dataArr =[];
  connect(collectionViewer: CollectionViewer): Observable<(any | undefined)[]> {
    this.subscription.add(collectionViewer.viewChange.subscribe(range => {   
      const startPage = this.getPageForIndex(range.start);
      const endPage = this.getPageForIndex(range.end);
      // console.log(startPage,endPage,this.getPageForIndex(range.start),this.getPageForIndex(range.end));
      for (let i = startPage; i <= endPage; i++) {
        this.fetchPage(i);
      }
    }));
    return this.dataStream;
  }

  disconnect(): void {
    this.subscription.unsubscribe();
  }

  private getPageForIndex(index: number): number {
    return Math.floor(index / this.pageSize);
  }

  private fetchPage(page: number) {
    if (this.fetchedPages.has(page)) {
      return;
    }
    this.filter_description = this.descriptionParam;
    // if (JSON.parse(localStorage.getItem("so_list"))!== null && typeof JSON.parse(localStorage.getItem("so_list"))!== undefined){
    //     this.filter_description = JSON.parse(localStorage.getItem("so_list")).description;
    //   }
    this.fetchedPages.add(page);
    this.rest.getDataSo((page * this.pageSize)+1
    ,JSON.parse(localStorage.getItem("so_list")).startingDate,
    JSON.parse(localStorage.getItem("so_list")).endingDate,
    this.filter_description ).subscribe(data => { 
        this.cachedData.splice(page * this.pageSize, this.pageSize,...data["salesOrder"]);
    // console.log(data);
    // this.cachedData.splice(pa  ge * this.pageSize, this.pageSize,...this.dataArr);
      this.dataStream.next(this.cachedData);
    });
  }
}