import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-so',
  template: `
   <router-outlet></router-outlet>
  `,
  styles: []
})
export class ListSoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
