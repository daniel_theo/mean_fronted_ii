import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ListSoRoutingModule } from './list-so-routing.module';
import { ListComponent } from './list/list.component';
import { ListSoComponent } from './list-so.component';

import { MaterialAppModule } from '../ngmaterial.module';
import { DetailComponent } from './detail/detail.component';
import { ScrollingModule,ScrollDispatchModule  } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [
  		ListComponent,
  		ListSoComponent,
  		DetailComponent
  				],
  imports: [
    CommonModule,
    ListSoRoutingModule,
    MaterialAppModule,
    ReactiveFormsModule,
    FormsModule,
    ScrollingModule,
    ScrollDispatchModule, 
  ]
})
export class ListSoModule { }
