import { Injectable } from '@angular/core';

import { NavigationStart, Router } from '@angular/router';

import { RestApiService } from './rest-api.service';

@Injectable({
  	providedIn: 'root'
})
export class DataService {

    message = '';
 	messageType = '';
    url= this.rest.link_url();
  // user: any;
    user: any = {};
    nameUser ='';
    keyUser ='';
    companyName = '';

  constructor(private router: Router, private rest: RestApiService) {
    this.router.events.subscribe(event => {
    	if (event instanceof NavigationStart) {
     		this.message = '';
        this.messageType = '';
    	}
    });
  }

  error(message) {
   	this.messageType = 'danger';
   	this.message = message;
  }

  success(message) {
   	this.messageType = 'success';
   	this.message = message;
  }

  warning(message) {
   	this.messageType = 'warning';
   	this.message = message;
  }

  // get profile
  async getProfile() {
    try {
        if (localStorage.getItem('token')) {
        // const data = await this.rest.get(
        //   `${this.url}/profile`,
        // );
        // this.user = data['user'];
            await this.rest.user_getProfile().subscribe((data) => {
                if(data['success']){
                    // this.token = localStorage.getItem('token-pos');
                    this.user = data['user'];
                    this.nameUser = this.user['name'].replace(
                                        /\w\S*/g,
                                        function(txt) {
                                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                                        }
                                     );
                    this.keyUser = this.nameUser.toUpperCase().slice(0,1) ;
                    this.companyName = this.user['company'].replace(
                                        /\w\S*/g,
                                        function(txt) {
                                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                                        }
                                     );

                }
            });
        // console.log(data['user']);
        // this.router.navigate(['/dashboard']);
        // this.user.push(data['user']);
        }
    } catch (e) {
      this.error(e);
    }
  }

  async getProfileStatus() {
    try {
      if (localStorage.getItem('token')) {
        const data = await this.rest.get(
          `${this.url}/profile`,
        );
        return data['user'].status;
      }
    } catch (e) {
      this.error(e);
    }
  }

  toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
  }

  async CheckPriceDiscGroupSti(itemNo, discGroup) {
    try {
      if (localStorage.getItem('token')) {
        // console.log(itemNo, discGroup);
        const data = await this.rest.get(
          `${this.url}/api-item/cek-customer-price-sti/${itemNo}/${discGroup}`,
        );
        const price = await data['item'][0].SalesPrice;
        // console.log(price);
        return  price;
      }
    } catch (e) {
      this.error(e);
    }
  }
}
