import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {
	MatAutocompleteModule,
  MatButtonModule,
	MatBadgeModule,
	MatBottomSheetModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDividerModule,
	MatExpansionModule,
  MatGridListModule,
  MatIconModule,
	MatChipsModule,
  MatInputModule,
  MatListModule,
	MatTabsModule,
  MatSelectModule,
  MatSidenavModule,
	MatSnackBarModule,
	MatProgressSpinnerModule,
  MatTableModule,
	MatToolbarModule,
	MatStepperModule,
	MatMenuModule
			} from '@angular/material';

const ExportedMatModules = [
			CdkStepperModule,
			MatAutocompleteModule,
		  MatButtonModule,
			MatBadgeModule,
			MatBottomSheetModule,
		  MatCardModule,
		  MatCheckboxModule,
		  MatDialogModule,
		  MatDividerModule,
			MatExpansionModule,
		  MatGridListModule,
		  MatIconModule,
			MatChipsModule,
		  MatInputModule,
		  MatListModule,
			MatTabsModule,
		  MatSelectModule,
		  MatSidenavModule,
			MatSnackBarModule,
			MatProgressSpinnerModule,
		  MatTableModule,
			MatToolbarModule,
			MatStepperModule,
			MatMenuModule
]


@NgModule({
  	imports: [...ExportedMatModules	],
  	exports: [...ExportedMatModules ]
})
export class MaterialAppModule { }
