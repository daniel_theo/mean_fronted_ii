import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { MaterialAppModule } from '../ngmaterial.module';
import { ErrorBlockedComponent } from './error-blocked/error-blocked.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialAppModule,
    FormsModule,
  ],

  declarations: [
    DashboardComponent,
    ErrorBlockedComponent
  ],
  entryComponents: [
    ErrorBlockedComponent
  ]
})
export class DashboardModule { }
