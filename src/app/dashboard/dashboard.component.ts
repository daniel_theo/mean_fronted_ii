import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { ErrorBlockedComponent } from './error-blocked/error-blocked.component';
import { ServiceWorkerModule , SwUpdate, SwPush} from '@angular/service-worker';
import { MatSnackBar } from '@angular/material';

import { MatDialog } from '@angular/material';
@Component({

  selector: 'app-dashboard',
  templateUrl: './dashboard/dashboard.component.html',
  styleUrls: ['./dashboard/dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  dataLclStorage:any={};
  // keyUser = "";
  // nameUser = "";
  // companyName ="";
  search: string;

  constructor(
    private router: Router,
    public data: DataService, 
    public dialog: MatDialog,  
    public snackBar: MatSnackBar,
    private swUpdate: SwUpdate) { }
  
  async ngOnInit() {      
    await this.data.getProfile();
     this.swUpdate.available.subscribe(event => {
      console.log(event);
       const snack = this.snackBar.open("A new version of this app is available ","Update");
       snack.onAction().subscribe(()=>{
         window.location.reload();
       })
    })
  	// if (await this.data.getProfileStatus() === true){
    if (await this.data.user.status === true){
  		const dialogRef = this.dialog.open(ErrorBlockedComponent, {
              width: '350px'
            });
  		this.data.user = undefined;
        localStorage.clear();
  	} else {
         this.router.navigate(['/dashboard']);
         // this.nameUser = this.data.toTitleCase(this.data.user.name);
         // this.keyUser = this.nameUser.toUpperCase().slice(0,1) ;
         // this.companyName = this.data.toTitleCase(this.data.user.company);
    }
  }

  async toProductList(event: any) {    
    try{
      (this.search !== undefined)?  
        await localStorage.setItem('product_list', JSON.stringify({description: this.search})) :
        await localStorage.setItem('product_list', JSON.stringify({description: ''}))
     ;
      this.router.navigate([`products/list/`]);
    }catch (error) {
        this.data.error(error['message']);
    }  
  }
}
