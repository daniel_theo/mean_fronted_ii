import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-error-blocked',
  templateUrl: './error-blocked.component.html',
  styleUrls: ['./error-blocked.component.css']
})
export class ErrorBlockedComponent implements OnInit {

  constructor(
    public data: DataService
  ) { }

  ngOnInit() {
  }

  // logout when reset password success
  login(){
    window.location.replace('/login');
  }

}
