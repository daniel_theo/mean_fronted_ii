import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { DataService } from '../data.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password/forgot-password.component.html',
  styleUrls: ['./forgot-password/forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  title = 'Reset Password';
  url= this.rest.link_url();
  token: any;
  email   = '';
  password = '';
  password1 = '';
  dataToken: any = {} ;
  dataEmail: any = {} ;

  constructor(
    private router: Router,
    private rest: RestApiService,
    public data: DataService,
    private activatedRoute: ActivatedRoute
  ) { }

  // get data profile
  ngOnInit() {
     this.activatedRoute.params.subscribe(res => {
          this.token = res['id'];
          this.forgotPassword();
        });
  }

  // get data profile
  async forgotPassword(){
        try {
            const data= await this.rest.get(
               `${this.url}/forgot-password/`+this.token
            );
            this.dataToken = data['token'];
            this.dataEmail = data['email'];
        } catch (error) {
            this.data.error(error['message']);
        }
    }

  // function reset password
   async resetPassword() {

      try {
          const data = await this.rest.post(
              `${this.url}/forgot-password/`+this.token,
              {
                password: this.password,
                confirm: this.password1

              },
          );
          if (data['success']) {
              await this.data.getProfile();
              this.router.navigate(['login'])
                .then(()=>{
                     this.data.success(
                         'Update password Succesfull '
                    );
                }).catch(error=> this.data.error(error));
            } else {
                this.data.error(data['message']);
            }
      } catch (error) {
          this.data.error(error['message']);
      }

    }

}
