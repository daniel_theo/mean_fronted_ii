import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

import { MaterialAppModule } from '../ngmaterial.module';

import { ErrorBlockedComponent } from '../modal-error/error-blocked/error-blocked.component';
import { ErrorLoginComponent } from '../modal-error/error-login/error-login.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    MaterialAppModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginComponent,
    ErrorBlockedComponent,
    ErrorLoginComponent
  ],

  entryComponents: [
    ErrorBlockedComponent,
    ErrorLoginComponent
  ],

})
export class LoginModule { }
