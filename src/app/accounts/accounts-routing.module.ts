import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegisterComponent } from './register/register.component';
import { AccountsComponent } from './accounts.component';
import { UserProfileComponent } from './user-profile/user-profile/user-profile.component';
import { UserProfileEditComponent } from './user-profile/user-profile-edit/user-profile-edit.component';


const routes: Routes = [
{
    path: '',
    component: AccountsComponent,
    children: [
      {
        path: 'reset-password/:id',
        component: ResetPasswordComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'user-profile',
        component: UserProfileComponent
      },
      {
        path: 'user-profile-edit/:id',
        component: UserProfileEditComponent
      }
    ]
}

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
