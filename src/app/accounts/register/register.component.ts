import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
		title = 'Register';

		name = '';
		email = '';
		password = '';
		password1 = '';
		company ='';
		dept_code ='';
		salesPerson = '';
		retailUser = '';
		role = '';
		url= this.rest.link_url();

		dataCompany: any;
		dataDept: any;
		dataRole: any;
		dataSalesPerson: any;
		dataRetailUser: any;

	constructor(
		private router: Router,
		public data: DataService,
		private rest: RestApiService,
	) {}

	async ngOnInit() {
		const dataExII= await this.rest.get(
			`${this.url}/api-settings/company/`
		);
		const dataExIII= await this.rest.get(
			`${this.url}/api-settings/department/`
		);
		const dataExIV= await this.rest.get(
			`${this.url}/api-settings/role/`
		);
		const dataExV= await this.rest.get(
			`${this.url}/api-customer/salesPerson-all`
		);
		const dataExVI= await this.rest.get(
			`${this.url}/api-accounts/retail-user/`
		);

		this.dataCompany = dataExII['company'];
		this.dataDept = dataExIII['dep'];
		this.dataRole = dataExIV['roles'];
		this.dataSalesPerson = dataExV['salesCode'];
		this.dataRetailUser = dataExVI['salesCode'];
	}

	async register() {
		try {
		const data = await this.rest.post(
			`${this.url}/api-accounts/signup`,
			{
				name: this.name,
				email: this.email,
				password: this.password,
				password1: this.password1,
				company: this.company,
				dept_code: this.dept_code,
				role: this.role,
				salesPerson: this.salesPerson,
				retailUser: this.retailUser
			},
        );
        if (data['success']) {
			await this.data.getProfile();
			this.router.navigate(['/accounts/user-profile'])
				.then(()=>{
					this.data.success(
						'Registration Succesfull..'
					);
				}).catch(error=> this.data.error(error));
			} else {
				this.data.error(data['message']);
			}

		} catch (error) {
			this.data.error(error['message']);
		}
	}
}
