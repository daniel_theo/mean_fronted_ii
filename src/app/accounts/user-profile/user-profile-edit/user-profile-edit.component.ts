import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { ActivatedRoute,Router } from '@angular/router';

// const BASE_URL = 'http://localhost:3030';

@Component({
    selector: 'app-user-profile-edit',
    templateUrl: './user-profile-edit.component.html',
    styleUrls: ['./user-profile-edit.component.css']
})
export class UserProfileEditComponent implements OnInit {

    UserId: any;
    dataUser: any = {} ;
    dataCompany: any;
    dataDept: any;
    dataRole: any;
    dataSalesPerson: any;
    dataRetailUser: any;
    title       = 'Edit User';
    url= this.rest.link_url();
    constructor(
        private router: Router,
        public data: DataService,
        private rest: RestApiService,
        private activatedRoute: ActivatedRoute ,
    ) {}

    ngOnInit() {
        this.activatedRoute.params.subscribe(res => {
            this.UserId = res['id'];
            this.editUser();
        });
    }

    // get data user profile
    async editUser(){
        try {
            const dataEx= await this.rest.get(
                `${this.url}/api-accounts/edit-user/`+this.UserId
            );
            const dataExII= await this.rest.get(
                `${this.url}/api-settings/company/`
            );
            const dataExIII= await this.rest.get(
                `${this.url}/api-settings/department/`
            );
            const dataExIV= await this.rest.get(
                `${this.url}/api-settings/role/`
            );
            const dataExV= await this.rest.get(
                `${this.url}/api-customer/salesPerson-all`
            );
            const dataExVI= await this.rest.get(
                `${this.url}/api-accounts/retail-user/`
            );
            this.dataUser = dataEx['user'];
            
            this.dataCompany = dataExII['company'];
            this.dataDept = dataExIII['dep'];
            this.dataRole = dataExIV['roles'];
            this.dataSalesPerson = dataExV['salesCode'];
            this.dataRetailUser = dataExVI['salesCode'];
        } catch (error) {
            this.data.error(error['message']);
        }
    }

    // edit user profile
    async SaveEdit() {
        try {
            const data = await this.rest.post(
                `${this.url}/api-accounts/edit-user/`+this.UserId,	this.dataUser
            );
            if (data['success']) {
                await this.data.getProfile();
                this.router.navigate(['accounts/user-profile'])
                .then(()=>{
                        this.data.success(
                        `Succesfull ...Update User `+this.dataUser.email
                    );
                }).catch(error=> this.data.error(error));
            } else {
                    this.data.error(data['message']);
            }

        } catch (error) {
            this.data.error(error['message']);
        }
    }

}
