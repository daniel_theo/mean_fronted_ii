import {Component, OnInit, ViewChild} from '@angular/core';

import { DataService } from '../../../data.service';
import { RestApiService } from '../../../rest-api.service';
import { Router } from '@angular/router';

import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
    toolbar : boolean = true;
    displayedColumns: string[] = ['Name', 'Email', 'Company','Departement', 'approveLimit1', 'approveLimit2','aproveDiscount', 'Action'];
	  dataUser :any =[];
    url= this.rest.link_url();

  	constructor(
      public data: DataService,
      private rest: RestApiService,
      private router: Router,
      public snackBar: MatSnackBar
    ) { }

    // get data profile
  	async ngOnInit() {
    	try {
      		const dataEx = await this.rest.get(
        		`${this.url}/api-accounts/accounts`
      		);
          this.dataUser = dataEx['user'];
          
    	} catch (error) {
      		this.data.error(error['message']);
    	}
  	}

    // delete user profile
    async deleteUser(id) {
      try {
        const res = await this.rest.delete(
            `${this.url}/api-accounts/delete-user/${id}`
        );

        if (res['success']) {
          await this.data.getProfile();
          this.router.navigate(['accounts/user-profile'])
          .then(()=>{
                    this.data.success(
                        'Delete User Succesfull..'
                    );
                }).catch(error=> this.data.error(error));
          this.ngOnInit();
        } else {
                this.data.error(res['message']);
            }

      } catch (error) {
        this.data.error(error['message']);
      }
    }

    // forgot password function
    async forgotpassword(id){
      try {
        const data = await this.rest.get(
            `${this.url}/api-accounts/forgot-password/${id}`
       );

      } catch (error) {
        this.data.error(error['message']);
      }
      this.snackBar.openFromComponent(SnackBarForgotComponent, {
            duration: 1000,
        });
    }


}

@Component({
  selector: 'snackbar-forgot-component',
  templateUrl: './snackbar-forgot/snackbar-forgot.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class SnackBarForgotComponent {}
