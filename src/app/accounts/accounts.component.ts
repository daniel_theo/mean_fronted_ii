import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accounts',
  template: `
   <router-outlet></router-outlet>
  `,
  styles: []
})
export class AccountsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}