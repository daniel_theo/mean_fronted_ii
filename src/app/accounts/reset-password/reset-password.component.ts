import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../rest-api.service';
import { DataService } from '../../data.service';
import { ActivatedRoute,Router } from '@angular/router';

import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalResetComponent } from '../../modal-error/modal-reset/modal-reset.component';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
    UserId: any;
    dataUser:any ={} ;
    title   = 'Reset Password';
	  email   = '';
  	password = '';
  	password1 = '';
    url= this.rest.link_url();

  	constructor(
    	private router: Router,
    	public data: DataService,
    	private rest: RestApiService,
      private activatedRoute: ActivatedRoute ,
      public dialog: MatDialog
  	) {}

  	ngOnInit() {
      this.activatedRoute.params.subscribe(res => {
          this.UserId = res['id'];
          this.editUser();
        });
  	}
    async editUser(){
        try {
            const dataEx= await this.rest.get(
              `${this.url}/api-accounts/change_password/`+this.UserId
            );

            this.dataUser = dataEx['user'];
        } catch (error) {
            this.data.error(error['message']);
        }
    }

  	async resetPassword() {
      try {
          const data = await this.rest.post(
            `${this.url}/api-accounts/change_password/`+this.UserId,
            	{
              	password: this.password,
              	confirm: this.password1

            	},
          );
          if (data['success']) {
              this.data.user = undefined;
              localStorage.clear();
            	const dialogRef = this.dialog.open(ModalResetComponent, {
                width: '350px'
              });
          	} else {
            		this.data.error(data['message']);
          	}
      } catch (error) {
        	this.data.error(error['message']);
      }

    }
}
