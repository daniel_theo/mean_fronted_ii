import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';

import { AccountsRoutingModule } from './accounts-routing.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegisterComponent } from './register/register.component';
import { AccountsComponent } from './accounts.component';
import { UserProfileComponent } from './user-profile/user-profile/user-profile.component';
import { UserProfileEditComponent } from './user-profile/user-profile-edit/user-profile-edit.component';
import { SnackBarForgotComponent } from './user-profile/user-profile/user-profile.component';

import { MaterialAppModule } from '../ngmaterial.module';
import { ModalResetComponent } from '../modal-error/modal-reset/modal-reset.component';

@NgModule({
  imports: [
    CommonModule,
    AccountsRoutingModule,
    MaterialAppModule,
    FormsModule
  ],
  entryComponents: [
    SnackBarForgotComponent,
    ModalResetComponent
  ],
  declarations: [
    ResetPasswordComponent,
    SnackBarForgotComponent,
    RegisterComponent,
    AccountsComponent,
    UserProfileComponent,
    UserProfileEditComponent,
    ModalResetComponent
  ],
})
export class AccountsModule { }
