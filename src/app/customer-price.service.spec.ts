import { TestBed } from '@angular/core/testing';

import { CustomerPriceService } from './customer-price.service';

describe('CustomerPriceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomerPriceService = TestBed.get(CustomerPriceService);
    expect(service).toBeTruthy();
  });
});
