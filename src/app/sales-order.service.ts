import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SalesOrderService {
	  dataLocalItem=[];
    dataLocalItemSubmit :any={};
    dataLocalGroup:any={};
    
    amount:any={};
  	dataLclStorage:any={};
  	dataGetLclStorage:any={};

    dataGetLclStorageDraft:any={};
    dataLclStorageDraft :any={};

  	chartQty = 0;

  	constructor() { this.chartQty = this.countItemChart()}

    // add to localStorage order draft with filter
  	itemLocalStorage(arr){
      try{
        this.dataLocalItem =[];
      	if (localStorage.getItem("order_draft") === null) {
          	// this.dataLocalItem.push(arr);
            this.dataLocalItem.unshift(arr);
          	this.dataLclStorage.item = this.dataLocalItem;
          	localStorage.setItem("order_draft",JSON.stringify(this.dataLclStorage));
        	}else{
          	this.dataGetLclStorage   = JSON.parse(localStorage.getItem("order_draft"));
            if (typeof this.dataGetLclStorage.item == "undefined"){
               // this.dataLocalItem.push(arr);
               this.dataLocalItem.unshift(arr);
            }else{
               this.dataLocalItem   = this.dataGetLclStorage.item.filter(e => e.itemNo !== arr.itemNo || e.loc !== arr.loc || e.var !== arr.var);
               // this.dataLocalItem.push(arr);
               this.dataLocalItem.unshift(arr);
            }
          	this.dataGetLclStorage.item = this.dataLocalItem;
          	localStorage.setItem("order_draft",JSON.stringify(this.dataGetLclStorage));
        	}
        	this.chartQty	= this.countItemChart();
        }catch(error){
          console.log(error.message);
        }
    }

    // remove item in localStorage with filter
    removeChart(arr){
      try{
        this.dataLocalItem =[];
      	this.dataGetLclStorage   = JSON.parse(localStorage.getItem("order_draft"));
        this.dataLocalItem   = this.dataGetLclStorage.item.filter(e => e.itemNo !== arr.itemNo || e.loc !== arr.loc || e.var !== arr.var);
        this.dataGetLclStorage.item = this.dataLocalItem;
        localStorage.setItem("order_draft",JSON.stringify(this.dataGetLclStorage));
        this.chartQty	= this.countItemChart();
      }catch(error){
        console.log(error.message);
      }
    }

    // total price product and add to localStorage
    countItemChart(){
      try{
      	this.dataGetLclStorage   = JSON.parse(localStorage.getItem("order_draft"));
      	if (this.dataGetLclStorage === null){
      		return 0;
      	}else{
          if (typeof this.dataGetLclStorage.item == "undefined"){
              return 0;
          }else{
      		    // return this.dataGetLclStorage.item.reduce((acc, cur) => acc + cur.qty, 0);
      		    return this.dataGetLclStorage.item.length;
          }
      	}
      }catch(error){
        console.log(error.message);
      }
    }

    // countItemAmountChart
    countItemAmountChart(){
      try{
        this.dataGetLclStorage   = JSON.parse(localStorage.getItem("order_draft"));
        if (this.dataGetLclStorage === null){
          return 0;
        }else{
          if (typeof this.dataGetLclStorage.item == "undefined"){
              return 0;
          }else{
              this.amount = 0;
              let deliveryService = 0;
              let coupon = 0
              // for(var key in this.dataGetLclStorage.item){
              //   this.dataLocalGroup=this.dataGetLclStorage.item[key].itemGroup;
              //   this.amount = this.amount+this.dataLocalGroup.reduce((acc, cur) => acc + (cur.qty*this.dataGetLclStorage.item[key].price), 0);
              // }
              this.amount =  this.dataGetLclStorage.item.reduce((acc, cur) => acc + (cur.qty*cur.price), 0);
              if (typeof this.dataGetLclStorage.deliveryService === "undefined"){
                deliveryService =0;
              }else{
                deliveryService = this.dataGetLclStorage.deliveryService;
              }
              if (typeof this.dataGetLclStorage.coupon === "undefined"){
                coupon =0;
              }else{
                coupon = this.dataGetLclStorage.coupon;
              }
              // console.log(this.amount, deliveryService);
              return this.amount + deliveryService - coupon  ;
          }
        }
      }catch(error){
        console.log(error.message);
      }
    }
     countItemAmountChartUSD(){
      try{
        this.dataGetLclStorage   = JSON.parse(localStorage.getItem("order_draft"));
        if (this.dataGetLclStorage === null){
          return 0;
        }else{
          if (typeof this.dataGetLclStorage.item == "undefined"){
              return 0;
          }else{
              this.amount = 0;
              for(var key in this.dataGetLclStorage.item){
                this.dataLocalGroup=this.dataGetLclStorage.item[key].itemGroup;
                this.amount = this.amount+this.dataLocalGroup.reduce((acc, cur) => acc + (cur.qty*this.dataGetLclStorage.item[key].price), 0);
              }
              // return this.dataGetLclStorage.item.reduce((acc, cur) => acc + (cur.qty*this.dataGetLclStorage.item[key].price), 0);
              return this.amount;
          }
        }
      }catch(error){
        console.log(error.message);
      }
    }

    // save to draft
    saveDraft(){
      try{
        let draftNo = Date.now();
        this.dataGetLclStorageDraft               = JSON.parse(localStorage.getItem("order_draft"));
        this.dataGetLclStorageDraft.totalAmount   = this.countItemAmountChart();
        this.dataLclStorageDraft = JSON.parse(localStorage.getItem("draft_List"));
        if(this.dataLclStorageDraft === null){
          this.dataLclStorageDraft ={
                [draftNo] : this.dataGetLclStorageDraft
          }
          localStorage.setItem('draft_List',JSON.stringify(this.dataLclStorageDraft));
        }else{
          this.dataLclStorageDraft[draftNo]    =  this.dataGetLclStorageDraft;
          localStorage.setItem('draft_List',JSON.stringify(this.dataLclStorageDraft));
        }
        localStorage.removeItem('order_draft');
        this.chartQty  = this.countItemChart();
      }catch(error){
        console.log(error.message);
      }
    }

    // Delete Order
    deleteOrder(){
      try{
        localStorage.removeItem('order_draft');
        this.chartQty  = this.countItemChart();
      }catch(error){
        console.log(error.message);
      }
    }

    selectDraft(arr,key){
      try{
        localStorage.removeItem('order_draft');
        localStorage.setItem("order_draft",JSON.stringify(arr));
        let dataDraftList = JSON.parse(localStorage.getItem("draft_List"));
        // let dataNew ={...dataDraftList};
        delete dataDraftList[key];
        // console.log(dataDraftList);
        localStorage.removeItem("draft_List");
        localStorage.setItem('draft_List',JSON.stringify(dataDraftList));
        this.chartQty  = this.countItemChart();
      }catch(error){
        console.log(error.message);
      }
    }
}
