import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './settings.component';

import { DepartmentComponent } from './department/department/department.component';
import { AddDepartmentComponent } from './department/add-department/add-department.component';
import { EditDepartmentComponent } from './department/edit-department/edit-department.component';

import { CompanyComponent } from './company/company/company.component';
import { AddCompanyComponent } from './company/add-company/add-company.component';
import { EditCompanyComponent } from './company/edit-company/edit-company.component';

import { RoleComponent } from './role/role/role.component';
import { RoleAddComponent } from './role/role-add/role-add.component';
import { RoleEditComponent } from './role/role-edit/role-edit.component';
import { RoleShowComponent } from './role/role-show/role-show.component';

const routes: Routes = [
 {
 	path: '',
    component: SettingsComponent,
     children: [
      {
        path: 'company',
        component: CompanyComponent
      },
      {
        path: 'company/add-company',
        component: AddCompanyComponent
      },
      {
        path: 'company/edit-company/:id',
        component: EditCompanyComponent
      },
      {
        path: 'department',
        component: DepartmentComponent
      },
      {
        path: 'department/add-department',
        component: AddDepartmentComponent
      },
      {
        path: 'department/edit-department/:id',
        component: EditDepartmentComponent
      },
      {
        path: 'role',
        component: RoleComponent
      },
      {
        path: 'role/role-add',
        component: RoleAddComponent
      },
      {
        path: 'role/role-edit/:id',
        component: RoleEditComponent
      },
      {
        path: 'role/role-show/:id',
        component: RoleShowComponent
      }
    ]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
