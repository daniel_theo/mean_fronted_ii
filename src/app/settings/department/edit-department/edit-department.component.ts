import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
	selector: 'app-edit-department',
	templateUrl: './edit-department.component.html',
	styleUrls: ['./edit-department.component.css']
})

export class EditDepartmentComponent implements OnInit {

	title = 'Edit Department';

	deptId: any;
	dataDepartment: any = {} ;
	url= this.rest.link_url();
	constructor(
		private router: Router,
		public data: DataService,
		private rest: RestApiService,
		private activatedRoute: ActivatedRoute ,
	) { }

	ngOnInit() {
	this.activatedRoute.params.subscribe(res => {
		this.deptId = res['id'];
		this.editDepartment();
		});
	}

	// get data department
	async editDepartment(){
		try {
			const dataEx= await this.rest.get(
			`${this.url}/api-settings/department/`+this.deptId
			);
			this.dataDepartment = dataEx['department'];
		} catch (error) {
			this.data.error(error['message']);
		}
	}

	// edit department
	async saveDepartment() {
		try {
			const data = await this.rest.put(
			`${this.url}/api-settings/department/`+this.deptId,  this.dataDepartment
			);
			if (data['success']) {
				await this.data.getProfile();
				this.router.navigate(['/settings/department'])
				.then(()=>{
						this.data.success(
						`Succesfull ...Update Department `+this.dataDepartment.code
					);
				}).catch(error=> this.data.error(error));
			} else {
					this.data.error(data['message']);
			}

		} catch (error) {
			this.data.error(error['message']);
		}
	}
}
