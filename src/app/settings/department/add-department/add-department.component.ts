import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css']
})
export class AddDepartmentComponent implements OnInit {
  title ='Add Department';

  code = '';
  description = '';
  status ='true';

  deps : any ;
  url= this.rest.link_url();
  constructor(private router: Router,
  public data: DataService,
  private rest: RestApiService
  ) { }

  async ngOnInit() {
    try {
      const data = await this.rest.get(
        `${this.url}/api-settings/department`,
      );
      data['success']
        ? (this.deps = data['deps'])
        : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);
    }
  }

  // add department
  async adddep() {
    try {
      const data = await this.rest.post(
        `${this.url}/api-settings/department`,
        {
          code: this.code,
          description: this.description,
          status: this.status
        }
      );
      if (data['success']) {
        this.router.navigate(['/settings/department'])
            .then(()=>{
                this.data.success(
                  'Add department Succesfull..'
                );
            }).catch(error=> this.data.error(error));
      } else {
              this.data.error(data['message']);
      }
    } catch (error) {
          this.data.error(error['message']);
    }
  }


}
