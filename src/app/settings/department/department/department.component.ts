import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  displayedColumns: string[] = ['Code', 'Description', 'Action'];
  dep = [];
  url= this.rest.link_url();
  constructor(private data: DataService, private rest: RestApiService, private router: Router) { }

  async ngOnInit() {
    try {
      const data = await this.rest.get(
        `${this.url}/api-settings/department`
      );
      data['success']
        ? (this.dep = data['dep'])
        : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);
    }
  }

  // delete department
  async deletedep(id) {
    try {
      const res = await this.rest.delete(
          `${this.url}/api-settings/department/${id}`
      );

      if (res['success']) {
        await this.data.getProfile();
        this.router.navigate(['/settings/department'])
        .then(()=>{
                  this.data.success(
                      'Delete department Succesfull..'
                  );
              }).catch(error=> this.data.error(error));
        this.ngOnInit();
    } else {
            this.data.error(res['message']);
        }

  } catch (error) {
    this.data.error(error['message']);
  }
  }

}
