import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';

import { CompanyComponent } from './company/company/company.component';
import { EditCompanyComponent } from './company/edit-company/edit-company.component';

import { RoleComponent } from './role/role/role.component';
import { RoleAddComponent } from './role/role-add/role-add.component';
import { RoleEditComponent } from './role/role-edit/role-edit.component';
import { RoleShowComponent } from './role/role-show/role-show.component';

import { MaterialAppModule } from '../ngmaterial.module';
import { AddCompanyComponent } from './company/add-company/add-company.component';

import { DepartmentComponent } from './department/department/department.component';
import { AddDepartmentComponent } from './department/add-department/add-department.component';
import { EditDepartmentComponent } from './department/edit-department/edit-department.component';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    MaterialAppModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [	SettingsComponent, 
  					RoleAddComponent, 
  					RoleEditComponent, 
            RoleShowComponent, 
  					RoleComponent,
  					CompanyComponent, 
  					AddCompanyComponent, 
  					EditCompanyComponent, 
  					DepartmentComponent, 
  					AddDepartmentComponent,
  					EditDepartmentComponent
  					]
})
export class SettingsModule { }
