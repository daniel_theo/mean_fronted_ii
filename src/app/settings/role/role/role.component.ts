import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  displayedColumns: string[] = ['Code', 'Description', 'Action'];
  roles = [];
  url= this.rest.link_url();
  constructor(private data: DataService, private rest: RestApiService, private router: Router) { }

  async ngOnInit() {
    try {
      const data = await this.rest.get(
        `${this.url}/api-settings/role`
      );
      data['success']
        ? (this.roles = data['roles'])
        : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);
    }
  }

  // delete role
  async deleteRole(id) {
    try {
      const res = await this.rest.delete(
        `${this.url}/api-settings/role/${id}`
    );

    if (res['success']) {
      await this.data.getProfile();
      this.router.navigate(['/settings/role'])
      .then(()=>{
                this.data.success(
                    'Delete role Succesfull..'
                );
            }).catch(error=> this.data.error(error));
      this.ngOnInit();
    } else {
            this.data.error(res['message']);
        }

  } catch (error) {
    this.data.error(error['message']);
  }
  }
}
