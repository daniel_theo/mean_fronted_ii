import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-role-show',
  templateUrl: './role-show.component.html',
  styleUrls: ['./role-show.component.css']
})
export class RoleShowComponent implements OnInit {
  displayedColumns: string[] = ['Code','Description', 'Action'];
  roleId: any;
  dataRole: any;
  dataAdmin: any;

  code      ='';
  description ='';
  resource  ={};
  methods   ={};
  url= this.rest.link_url();
	constructor( public data: DataService, private rest: RestApiService, private activatedRoute: ActivatedRoute , private router: Router) { }
	 ngOnInit() {
        this.activatedRoute.params.subscribe(res => {
          this.roleId = res['id'];
          this.editRole();
        });
    };

    // get data role
    async editRole(){
        try {
            const dataRole = await this.rest.get(
              `${this.url}/settings/show-role/`+this.roleId
            );
            this.dataRole = dataRole;
        } catch (error) {
            this.data.error(error['message']);
        }
    }
    objectKeys() {
      return Object.keys(this.dataRole.roleGroup[0].permissions);
    };

    objectKeysII(obj) {
      return Object.keys(obj);
    };




}
