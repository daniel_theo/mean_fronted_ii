import { Component, OnInit } from '@angular/core';

import { DataService } from '../../../data.service';
import { RestApiService } from '../../../rest-api.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-role-add',
	templateUrl: './role-add.component.html',
	styleUrls: ['./role-add.component.css']
})
export class RoleAddComponent implements OnInit {
	displayedColumns: string[] = ['Code','Description', 'Action'];
	dataRole: any;
	code      ='';
	description ='';
	resource  ={};
	methods   ={};
	url= this.rest.link_url();
	
	constructor( 
		public data: DataService, 
		private rest: RestApiService, 
		private router: Router) 
	{ }
	
	async ngOnInit() {
		// try {
		// 	const dataRole = await this.rest.get(
		// 	`${this.url}/api-settings/add-role`
		// 	);
		// 	this.dataRole = dataRole;
		// 	console.log(dataRole)
		// } catch (error) {
		// 	this.data.error(error['message']);
		// }
	};

	// add role
	async saveRole(){
		try {
			const data = await this.rest.post(
				`${this.url}/api-settings/role`,   {
					code: this.code,
					description: this.description,
					resource: this.resource,
					methods: this.methods
				}
			);
		if (data['success']) {
			await this.data.getProfile();
			this.router.navigate(['/settings/role'])
				.then(()=>{
					this.data.success(
						'Add role Succesfull..'
					);
				}).catch(error=> this.data.error(error));
			} else {
				this.data.error(data['message']);
			}

		} catch (error) {
		this.data.error(error['message']);
		}
	};
	objectKeys() {
		return Object.keys(this.dataRole.roleGroup[0].permissions);
	};
}
