import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.css']
})
export class RoleEditComponent implements OnInit {
  displayedColumns: string[] = ['Code','Description', 'Action'];
  roleId: any;
  dataRole: any;

  code        ='';
  description ='';
  resource    ={};
  methods     ={};
  url= this.rest.link_url();

	constructor( public data: DataService, private rest: RestApiService, private activatedRoute: ActivatedRoute , private router: Router) { }
    ngOnInit() {
        this.activatedRoute.params.subscribe(res => {
          this.roleId = res['id'];
          this.editRole();
        });
    };

    // get data role
    async editRole(){
        try {
            const dataRole = await this.rest.get(
              `${this.url}/api-settings/role/`+this.roleId
            );
            this.dataRole = dataRole['role'];
            this.code = this.dataRole['code']
            this.description = this.dataRole['description']
        } catch (error) {
            this.data.error(error['message']);
        }
    }

    // add role
    async saveRole(){
      try {
          const data = await this.rest.put(
            `${this.url}/api-settings/role/`+this.roleId,   
            { code: this.code,
              description: this.description
            }
          );
        if (data['success']) {
            await this.data.getProfile();
            this.router.navigate(['/settings/role'])
              .then(()=>{
                  this.data.success(
                    'Add role Succesfull..'
                  );
              }).catch(error=> this.data.error(error));
          } else {
              this.data.error(data['message']);
          }

      } catch (error) {
        this.data.error(error['message']);
      }
    };
    objectKeys() {
      return Object.keys(this.dataRole.roleGroup[0].permissions);
    };
}
