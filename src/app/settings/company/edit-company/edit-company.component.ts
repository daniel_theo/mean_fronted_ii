import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { ActivatedRoute,Router } from '@angular/router';
//const BASE_URL = 'http://localhost:3030';
@Component({
	selector: 'app-edit-company',
	templateUrl: './edit-company.component.html',
	styleUrls: ['./edit-company.component.css']
})
export class EditCompanyComponent implements OnInit {

	title = 'Edit Company';

	companyId: any;
	dataCompany: any = {} ;
	url= this.rest.link_url();
	constructor(
		private router: Router,
		public data: DataService,
		private rest: RestApiService,
		private activatedRoute: ActivatedRoute ,
	) { }

	ngOnInit() {
	this.activatedRoute.params.subscribe(res => {
		this.companyId = res['id'];
		this.editCompany();
		});
	}

	// get data company
	async editCompany(){
		try {
			const dataEx= await this.rest.get(
				`${this.url}/api-settings/company/`+this.companyId
			);
			this.dataCompany = dataEx['company'];
		} catch (error) {
			this.data.error(error['message']);
		}
	}

	// edit company
	async saveCompany() {
		try {
			const data = await this.rest.put(
				`${this.url}/api-settings/company/`+this.companyId,  this.dataCompany
			);
			if (data['success']) {
				await this.data.getProfile();
				this.router.navigate(['/settings/company'])
				.then(()=>{
						this.data.success(
							`Succesfull ...Update Company `+this.dataCompany.code
					);
				}).catch(error=> this.data.error(error));
			} else {
					this.data.error(data['message']);
			}

		} catch (error) {
			this.data.error(error['message']);
		}
	}
}
