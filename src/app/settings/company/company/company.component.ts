import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  displayedColumns: string[] = ['Code', 'Description', 'Address', 'Address1', 'Action'];
  company = [];

  constructor(private data: DataService, private rest: RestApiService, private router: Router) { }
  url= this.rest.link_url();

  async ngOnInit() {

    try {
      const data = await this.rest.get(
        `${this.url}/api-settings/company`
      );
      data['success']
        ? (this.company = data['company'])
        : this.data.error(data['message']);
    } catch (error) {
      this.data.error(error['message']);
    }
  }

  // delete company
  async deletecompanies(id) {
    try {
      const res = await this.rest.delete(
            `${this.url}/api-settings/company/${id}`
    );

    if (res['success']) {
      this.router.navigate(['/settings/company'])
      .then(()=>{
                this.data.success(
                    'Delete Company Succesfull..'
                );
            }).catch(error=> this.data.error(error));
      this.ngOnInit();
    } else {
            this.data.error(res['message']);
        }

  } catch (error) {
    this.data.error(error['message']);
  }
  }

}
