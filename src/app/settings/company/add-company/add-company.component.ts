import { Component, OnInit } from '@angular/core';

import { RestApiService } from '../../../rest-api.service';
import { DataService } from '../../../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {

  title = 'Add Company';
  toolbar = 'true';

  code = '';
  description = '';
  address = '';
  address1 = '';
  phone = '';
  vatregistration = '';
  vatname = '';
  vataddress = '';
  vataddress1 = '';
  vatinvoiceauthorizedname = '';
  vatinvoiceauthorizedposition = '';
  status = 'true';

  companies : any ;
  url= this.rest.link_url();
  constructor(
    private router: Router,
    public data: DataService,
    private rest: RestApiService,
  ) { }

  async ngOnInit() {
  }

  // add company
  async addcompany() {
    try {
      const data = await this.rest.post(
          `${this.url}/api-settings/company`,
        {
          code: this.code,
          description: this.description,
          address: this.address,
          address1: this.address1,
          phone: this.phone,
          vatregistration : this.vatregistration,
          vatname: this.vatname,
          vataddress: this.vataddress,
          vataddress1: this.vataddress1,
          vatinvoiceauthorizedname: this.vatinvoiceauthorizedname,
          vatinvoiceauthorizedposition: this.vatinvoiceauthorizedposition,
          status: this.status
        }
      );
      if (data['success']) {
      this.router.navigate(['/settings/company'])
          .then(()=>{
              this.data.success(
                'Add Company Succesfull..'
              );
          }).catch(error=> this.data.error(error));
        } else {
            this.data.error(data['message']);
        }
        } catch (error) {
          this.data.error(error['message']);
        }
    }

}
